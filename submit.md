---
title: submissions
layout: default
permalink: /submit
---

<script>
 MIN_FERMATAS = 60;
 MAX_FERMATAS = 80;
 MIN_FERMATA_SCALE = 0.01;
 MAX_FERMATA_SCALE = 0.15
</script>

# Submissions

We are currently accepting unsolicited proposals for our series of computational poetry books. We are particularly interested in proposals for works which leverage our ability to print uniquely generated copies of books. We believe technology in art should transcend gimmickry and deeply inform the work to the point of inseparability. We believe that generative poetry works best as a way of expressing human artistry in surprising and serendipitous ways, so are not currently interested in works that are primarily driven by the creativity of large language models. (But you are free to try to change our mind.)

We strive to give our authors equitable publishing contracts; all authors receive a payment upon signing an agreement, another payment upon delivering a manuscript, and receive 50% of net proceeds from the first sale.

Please note that we exclusively publish works licensed under copyleft licenses like the [Creative Commons BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/) license. Our authors retain copyright ownership of their works, but are required to release their works under such licenses. Computational works must also have their source code publicly available under an open source license.

If you are working on something you think we might be interested in, please drop us a line at <a href="mailto:editors@nothing-to-say.org?subject=PITCH%3A%20My%20short%20description">editors@nothing-to-say.org</a> with the subject line "PITCH: my short description". We'll do our best to get back to you quickly and honestly.

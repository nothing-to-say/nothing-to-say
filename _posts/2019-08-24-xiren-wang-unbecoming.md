---
layout: post
title: "Unbecoming Nina: Luminous, Without Skin"
author: "xiren wang"
author_slug: "xiren-wang"
date: 2019-08-24
permalink: /xiren-wang/unbecoming-nina
external_thumbnail_description: "Actors are taught extreme vulnerability, but not about those lined up to abuse it."
---

<style>
 .section-break {
     margin-top: 3em;
     margin-bottom: 3em;
     text-align: center;
 }

 .floating-quote {
	 margin-left: 16%;
	 font-style: italic;
	 text-align: justify;
 }
 .bold-section-start:first-letter {
     font-size: 50px;
 }
 .bold-section-start:first-line {
     font-weight: bold;
     line-height: 25px;
 }

</style>


<div class="section-break">~</div>

### _“As an actor, you can be anything you want.”_

I had just turned 21, and this was the greatest lure I had ever heard up until then. I was in LA, at a callback with an acting school admissions counselor, and this conversation changed the course of my life.

But acting school wasn’t so much about becoming who I thought I wanted to be, as it was about being stretched in a million ways to become what was written on the page, whatever that role happened to be. A Meisner instructor said, an actress should have a range from a lady to a whore. We were trained to be open where most people are closed; we were trained to say yes when most people would say no; we were trained to trust the unknown when most people never risk stepping onto something that wasn’t 100% solid ground. These new dimensions of being and living felt both cathartic and euphoric when I first discovered them. I fell in love with this magic. Only in hindsight do I see how dangerous it can be.

In acting school, the roles I studied, along with the roles I was cast to play were mostly rooted in darker, troubled girls. Perhaps it’s how I looked, or how I read on camera, or perhaps it’s because children who have been forced to grow up too quickly too soon all share the same osmosis of unnatural environmental responses that both caused and affected our misalignments and overcompensations. Children of dislocation all loop the same stories in our pathology, and share pain points a trained eye can see. Acting coaches have this trained eye. Psychologists have this trained eye. Predators have it too. The latter tend to be disarmingly charming.

When I first studied theatre, I didn’t truly understand the depths of what they meant by “theater is a safe place to do the things that are unsafe to do in real life.” It speaks of both privilege and innocence that I didn’t know how dangerous life could be among familiar faces. It speaks of naivety that I wasn’t aware of how young artists are lured into crime as pawns and victims. It speaks of ego that I never thought the crimes I vaguely read about in the periphery would ever happen to me. So I only took that line at face value and trusted the space where make-believe was happening. You don’t know what you don’t know.

<div class="section-break">~</div>

<p class="bold-section-start">Every female student at the acting conservatory starts her journey into acting with Chekhov’s Seagull. Nina, a would-be actress created in St. Petersburg in 1895 would be reborn again and again through each incoming would-be actress. She is our common denominator, regardless of where we came from. Once we’ve chosen the same trajectory, the contours of our paths end up looking alike. But Nina isn’t just an actress. The endurance of her character and the play speak of more common human experiences. Nina is universal in all of us who are young, full of wonder, in love with love, and infatuated with all that glitters. Her naivety and flaws are mirrored in us, too. So it doesn’t take much to become her.</p>

<h4 class="floating-quote">“...He does not believe in the theatre; he used to laugh at my dreams, and little by little I lost hope and ceased to believe. Then came all the troubles of love, the constant anxiety... I never knew what to do with my hands, and I couldn’t move properly or control my voice...”</h4>

These lines poured out of all of us as easily as our tears rolled down in Meisner classes. Everyone gets there. Everyone breaks down and a few will break through. Acting can be therapeutic, and although it is easily mistaken as therapy for new actors, it is not self serving in the way therapy is meant for. Scene study classes force us to search for the subtext, locate suffering and identify core objectives in the way few of us do in real life. I’m not sure why such a disconnect exists, but perhaps because confrontation is too painful, and few of us take the journey inwards. 

It is much easier to locate someone else’s pain and truths than it is to face our own, so life on stage is only more thrilling because actors carry through behaviors that are mostly muted by fear in real life. We were told many times that people pay good money to see and experience our vulnerability, so we must be more honest and far more emotionally available than non actors. So we keep dialing our vulnerability up, we go to dark places and call it emotional preparation, we become more raw than ever, but no one ever told us that we should turn this off in real life.

Was I the only one to have missed the memo?

<div class="section-break">~</div>

<h4 class="floating-quote">“I’ve changed now. I am a real actress. I act with joy, with exaltation. I am intoxicated by it and I feel that I am luminous.”</h4>

<p class="bold-section-start">At 25, I was still Nina. Luminous, without skin, and a 180 from the girl who once shielded herself from strangers. I let Nina live in me, but little did I know, Nina is the archetypal prey that perverse minds are trained to look for. Lolita is another type, as is Alice in Closer. They all have very similar scripts, scripts we all take turns reading. They all carry very similar pain. If we are ever a little bit like them, we carry this scent of pain, too, and there are always people looking for us. Perhaps that’s why real life is so dull and closed off. It’s protection, something I only understood from its absence. School was a safe space, and being without skin was what made us stars.</p>

Acting school, as our instructors have said, is the unlearning of what society has taught us. But the undoing of that can also erode our ability to recognize red flags and alarms in real life. Vulnerability is brutally potent and disarmingly lethal. So we find easy acceptance in the wrong people, mistaking their webs as safety and taking comfort in the scripts they feed us—the scripts that tell us what to think of life and of ourselves. Because when we’ve let dark characters live in us too frequently too long, we tend to know darkness better than we know who we are without them. We become prey. But there’s a part of us that think we are invincible. How delusional.

Transitioning back to real life is both assaulting and depressing. No more classes to fill the days, and no more scripts to memorize at night. No more showcases and sets were harder and harder to be on. You learn that no one wants to hear your monologues and you’re one of thousands of prettier girls who are competing for the same insignificant role. You’re lucky if you have more than a few words to say. You’re fortunate to even be typecast. You can be so extremely close to a booking, but being released from a hold still stings after years of rejection. Some people wander and float for years without representation. I was fortunate to have had agents, even still, empty days far outnumbered working days. All this breaks us a little bit. Then we self sabotage by dating toxic people, because when we only know darkness, we are drawn to darkness. My eyes roll at artists’ memoirs that detail their dangerous relationships. Why must we become such pathetic cliches? If we are so common in our trajectories, what have we all missed? Surely, no one signs up for victimhood.

It is too common a struggle to find our sense of placement in this world. It becomes even more difficult trying to reposition ourselves back into real life, after acting school has successfully skinned us. Damsels in distress, we’ve become, and in come a slew of “helping” hands.

<div class="section-break">~</div>

<p class="bold-section-start">The first con artist I met was a self proclaimed famous photographer who wanted to manage me. He was literally camped outside the acting conservatory waiting for prey. But there are more, often familiar faces, who in the name of protection, would slowly break us down and never let us break through. No one told us that monsters look charming. In acting school, the sociopaths we studied never carried such charisma, or a familiar face. It turns out, many lessons were missed. We knew what “tortured artists” were, as we often embodied them ourselves. But if we stay tortured long enough, we will meet “helpful hands” who reach us through familiar friends, often female friends, to make us feel safe. These hands want nothing more than our flesh to consume and trade, though it never is so crude upfront.</p>

If we had any ounce of talent or good looks at all, these hands reach us and confuse us by muddying the lines between admiration, friendship, mentorship, and guidance. National Geographic’s production of Picasso also had these hands, and watching the episodes can get very uncomfortable. There are Ninas littered throughout history, the art world, and our very real world. These hands poise as intellectual allies and confidants. These hands are there to hold us when we are shaken, so we come to trust these hands that would later exploit us, because flesh is a bigger business than art.

Acting school is a paradox of a place, because acting is the study of people and of life, yet contained in a school, it becomes insulated and safe, in all the ways real life isn’t. Why don’t we study criminals? Because they surely study us.

Every role, every engagement is like a trustfall. In school, we are carried; in life, we mostly fall. What becomes the life of someone who is used to being carried?

I fell hard. Not in love. In darkness. And I almost paid my life.

<div class="section-break">~</div>

<p class="bold-section-start">When we are guided by “helping hands,” it is easy to become corrupt. Even worse than being taken advantage of, we self-sabotage by choosing to do what the hands would prefer us do, rather than trusting or maintaining our own sense of direction. When we want to please those hands that derail us, this is the ultimate corruption of our own character, because we can no longer point to anyone else for our own downfall. But everything can be rebuilt. What we’ve become, we can un-become. Becoming Nina was easy. Unbecoming her is most difficult, but it’s a much better life.</p>

I didn’t know how much of a textbook prey I had been until I started to read into criminal profiles and con artists’ trappings. Any reading of the Epstein scandal feels chillingly familiar. I never met him personally, but I know of people that look like him, smell like him, and operate just like him. Of course, every girl is a version of Nina, Lolita, and Alice. He got away for so long  because he’s not the only one with the habit of young flesh. He got away for so long because there is never a shortage of Ninas around. Then there are rings and industries and even countries devoted to revering and protecting monsters like him. Monsters have long been a protected class. Except this lesson is missing in schools.

With every incoming class of acting students, a new class of Ninas are being groomed. Outside school, there exists another class of characters waiting to take Nina to a place that is no longer safe, and they do so by feeding her new scripts. Just like there is a universal Nina in all of us, there is a universal process among the bad characters, too. One of the earliest common tricks they use is to make us feel safe and accepted, so our defenses are down. Then, they start to feed us scripts that again, puts us in a process of unlearning. Only once we unlearn what is standing in the way, can there be space for us to learn what they need to teach us. Most of us go through this process unknowingly. Because most of us in our 20s are unknowing. Unknowing and hungry.

And that hunger tunes our scripts, so they end up sounding the same. Nina lives in all of us. Every time we are fed a script, our boundaries of intimacy shift. Our world changes. I have known exceptional women who were exploited by the very people who warned them their talents will be exploited. It’s not the obviously bad people we need to be scared of. It’s the ones pretending to be our saviors and mentors. The ones verbalizing their protection of us. The more painful stories break these days, the more this is confirmed. The more we live, the more universal our tracks are. But just as we were trained to unlearn and become, we can, too, learn and unbecome. Except for this time, we get to decide who we are. No more assigned identities. No more “helping hands.” We get to decide. 

Art comes from life, and is its own version of a survival script. It is often in survival mode, we befriend bad characters, and we take whatever script they have ready to feed us. Imagine if we analyzed their scripts the way they analyzed ours? Would our stories change? My experience is yes. My hope is yes. If we ever wanted a different story than the one that keeps us the victims, it has to be yes.

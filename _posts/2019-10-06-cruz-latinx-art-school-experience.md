---
layout: post
title: "A Latinx Art School Experience"
author: "ingrid cruz"
author_slug: "ingrid-cruz"
date: 2019-10-06
permalink: /ingrid-cruz/latinx-art-school-experience
external_thumbnail_description: "Difficulties approaching art school from a low-income Latinx background"
---

<style>
 .section-break {
     margin-top: 3em;
     margin-bottom: 3em;
     text-align: center;
 }
</style>


I’m jealous of college students today who have a better vocabulary for discussing the injustices they face. You know, _checking your privilege_, _food-insecure_---these terms just weren’t around before. 

I know I’m privileged to have been able to study art. I now make a living as a freelance writer while occasionally doing commissioned work. I continue to practice art as often as I can, and sometimes my budget and the universe align so I can travel. 

<div class="section-break">////</div>

As a child I mostly saw murals in East L.A. or Boyle Heights in the off chance that I had a reason to visit. To be honest, outside of art class and friends who kept sketchbooks full of graffiti, I was exposed to little art, [like other Latinxs](https://sites.ed.gov/hispanic-initiative/files/2015/03/Hispanics-and-Arts-Education-Factsheet-Final-121814.pdf) belonging to the working-class. 

I know one of my uncles in El Salvador was a talented artist and now works as a graphic designer, so I was lucky to have a supportive family that didn’t force me to study law, medicine, or engineering (the acceptable choices for first-generation immigrants). 

I remember going to museums few times as a child, and not knowing that art supplies existed outside of _Daria_. I had an awesome gel pen collection, Sharpies, and some pencils with different lead types. I made do with what I had and several of my high school art teachers were supportive of my creative pursuits. My mom said she thought I’d be good at drawing comics, and she bought me a book about cartooning from a Scholastic book fair.

But as for colleges, there was little we knew about scholarships, the difference between community college or four year universities, portfolios, internships or even summer camps. One school was meant to be as good as the next. 

I only knew public art, and maybe a little bit about photography. My uncle would occasionally send me art supplies or books as gifts. I checked books out from the library and gave myself self-imposed homework. I knew college would be expensive, but there were small moments that truly revealed how little I knew about the art world.

<div class="section-break">//// ////</div>

When I took my first painting class in college, I remember being baffled by the cost of supplies. That’s because college was the first time I walked into a _real_ art supply store. Up until then, my art supplies came from whatever I could find at a regular Target school supply section.

I didn’t have a car, but I was able to hitch a ride to an art supply store because it was cheaper than anything sold on campus. I had a roommate who was in the same class and I remember we both pitched it to buy acrylic paints. It’s been roughly 15 years since then, but I just remember I couldn’t have taken the class if I had had to bear the cost of these paints on my own. 

My first job in college was with Jumpstart, which would’ve been cooler if I had wanted to be an educator in the future, but it was still a fun job. My mom also had a small jewelry business that was run by my aunt while I was in college.

Helping my aunt out for the weekend meant using scarce public transport in Irvine, taking a commuter train, and then taking a metro from Union Station to the job, and doing the same thing all over again except not always knowing if there would be a bus that would be able to pick me up because some bus lines didn’t operate at the weekend in Irvine, or they stopped after 6 pm. 

Eventually I stopped this job because getting there was a transportation nightmare for me. That also meant having less money to spend and save, so buying anything was tough for me. 

<div class="section-break">//// //// ////</div>

I took a photography class that was one of my first experiences having another Latina classmate for the first time for a college class in my major. Though my teacher and her TA were cool, and so were many of my classmates, I learned quickly that analog photography costs would eat me alive.

There was paper to buy (and keep refrigerated), film rolls, lab fees, and an SLR. I couldn’t really concentrate on making cool concepts when I was too busy juggling finances and wondering just how much every exposure cost, but I powered through somehow. It wasn’t until I went to an art show at Cal-State University Long Beach (CSULB) that I realized we barely studied any people of color in the class. 

My photographer teacher asked us to visit a photography exhibit as a part of our class, and we were to discuss it later. Expecting nothing, I walked in so I could look at art just because I wanted to complete my homework assignment.

There, staring straight at me, were photos by Mexican photographer Graciela Iturbide. She was phenomenal. She captured life in Oaxaca in a way that truly showed people’s dignity. Her angles and compositions were amazing, her lighting was perfect. So why weren’t we studying her in class? 

I could tell my photography teacher was progressive. She tried to be inclusive by showing us work by photographers like Gordon Parks and Dianne Arbus, but I don’t think she went far enough. Our school had a large Asian and Asian-American population, and I don’t remember studying any Asian or Asian-American photographers, let alone Latinxs or Latin-Americans. 

<div class="section-break">//// //// //// ////</div>

I didn’t get along with my writing teacher, whom I’ll call Mr. C (he gave me my first C). 

For one assignment he had us write an art manifesto, which we were to read aloud for the class. He asked everyone to print out copies for the entire class, his reasoning being that our manifestos would be long, so it would be nice if everyone had a copy so they could continue on if we didn’t have time to read them all.

This was in 2007 or something—12 years ago. If my memory serves correctly, there were at least 25 students, maybe 30. Each essay was an average of 3 to 4 pages. 

My school’s art department gave us 30 free pages to print per month and I had a printer whose ink had run out. What’s more, I had other essays to write, and wasn’t about to pay for whatever it cost to print out copies for everyone. Sure, it was going to run me about 10 cents a page if I was able to somehow find a place that cheap. 

Except that I had maybe $60 total in my bank account. $10-20 was a lot of money for me. I told him so, and the only thing I remember was showing up with a copy of my essay for him alone. I got a bad grade. 

I was a terrible writer back then, and that I can live with. I just remember an angry face from him. I didn’t read my manifesto in class, and I’m unsure if he took points out for that because I wasn’t a part of the discussion. 

Later, a classmate confessed that she went to Kinkos to print out her statement and put that on her credit card. I remember she spent more than $20 on this assignment. 

I obviously didn’t expect him to hand me money, but he didn’t care at all when I told him this was a significant expense for me. In this class there were few Latinxs, many Asian Americans, and no black students, and here was yet another white male professor who didn’t understand that living paycheck-to-paycheck as a Latina isn’t the same as it is for white students. 

By my senior year, I was sneaking into the school cafeteria once a week during my winter quarter. I started dumpster diving for food and recyclables I could exchange for money at Ralph’s and Albertson’s. Sometimes I found random things I was able to sell, like a working vacuum cleaner, small pieces of furniture I’d advertise on Craigslist, etc. Even selling for $5 was a profit. 

I didn’t tell my professor that my $8-an-hour job wasn’t cutting it. It hurt to part with $20 because I was literally making money off people’s recyclables.

<div class="section-break">//// //// //// //// /////</div>

I’m not alone in experiencing these barriers in the art world. Latinx creators of all disciplines have many stories similar to mine. Things seem to have improved for the current generation as many students have worked hard to create a cultural shift that at least allows them to discuss their hurdles as opposed to keeping everything bottled up inside. But what can we do to change circumstances for Latinx and other minority artists?

Until long-term systemic changes (hopefully) improve access and equity for Latinx students, there are definitely things faculty, staff, and administrators can do to make things easier for them.

Students should be taught how and empowered to seek support when they need it. Mr. C wasn’t doing anything terrible, but he was either oblivious or insensitive to the financial difficulties he was imposing on me. Maybe if I had known that it’s okay to speak up, I would have felt more comfortable confronting him. Maybe I would have spoken with someone in the admin office and asked for help navigating the situation. None of these things occurred to me at the time.

Non-white artists should be included in currulicums year-round. Many museums, schools, and other institutions celebrate Black History Month, Latinx/Hispanic Heritage Month, and Asian Pacific Islander Heritage Month. Some professors even include Native Americans. But artists of color shouldn’t just be filler material for these specific times of the year. We work very hard all year long and hone our skills every day.

The Smithsonian Institute’s Learning Lab has [a few materials](https://learninglab.si.edu/search?st=hispanic+heritage&item_type=collections&st_op=and) about Latinx artists. The National Gallery of Art provides [a list of African-American artists](https://www.nga.gov/features/african-american-artists.html), and finding artists of various historically oppressed groups has never been easier. It would behoove all institutions to include artists of all backgrounds even if not all universities are diverse.

A recent study shows that even though Latinx students are [attending college](https://www.pbs.org/newshour/education/more-hispanics-are-going-to-college-and-graduating-but-disparity-persists) at higher rates, the amount of Latinx professors in academia [isn’t keeping up](https://diverseeducation.com/article/87181/).  However, colleges do often have budgets for guest lecturers, art exhibits, and other events. Recruiting Latinx and other professors who are underrepresented in academia to fill these roles can improve visibility and fill in gaps for universities that are unable to hire art professors from underrepresented communities while they come up with sustainable long-term hiring strategies
Most artists tend to lean toward being moderate or progressive, but school administrators should consider adding racial and class sensitivity training to other onboarding programs already in existence. Training and workshops about class and race can help Latinx students adjust to their new reality. At the very least, equipping educators would make it easier to start difficult conversations about how race, class, and privilege can make art school even more challenging for underrepresented students.

Small steps like these won’t solve all injustices minority students face, but they can create a better environment for everyone. In a way, this essay is one small way I can contribute to positive changes. I can’t change the past, but sharing lessons I learned about how inequality and race affected my education may help others know that they _do_ belong to the academic institutions that accept them into their programs. 

I had few role models to turn to at school who had been through what I went through. Those of us who understand how cruel academia can be are the best at advising about big or small steps that can be taken to improve the educational experiences of underrepresented students. 

College is supposed to be rigorous and help students grow professionally and personally, but it shouldn’t continue to add barriers that perpetuate inequality. Obviously, the biases I faced won’t be wiped out overnight, but the implementing even small-scale solutions can have a big impact and allow the next generations to continue solving problems as they come.

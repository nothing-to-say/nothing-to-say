---
layout: post
title: "who exactly are museum internships for?"
author: "erin farrell"
author_slug: "erin-farrell"
date: 2019-01-19
permalink: /erin-farrell/internships.html
external_thumbnail_description: "Erin Farrell writes from outside the gates of the gallery world. Let's talk about internships and money."
---

<style>
 .section-break {
     margin-top: 3em;
     margin-bottom: 3em;
     text-align: center;
 }

 .right-aligned-emphasis {
     text-align: right;
 }
</style>


Since graduating from college I have found myself sitting in a predictable stew of anxiety. I break into a cold sweat just thinking about what the Hell I am going to do with myself over the coming days and months, let alone years. Since leaving the comforts of school, my freefall into the uncertainties of early adulthood has begun. I chose to deal with my current situation in those most pragmatic way I can devise: apply to everything and take whatever job sticks. Perhaps I am hoping that a random hiring manager will decide what I do with my life, and, to be honest, the stressed out and nail-biting version of myself is sort of okay with that. Though I can resign some things to fate, complacency is impractical in this situation. So applying to jobs in fields that interest me is as good a place as any to begin. 

I am lucky to have many passions. I double-majored in college in both Art History and Film Studies and even dabbled with a Global Black Studies minor until a professor warned me I might be taking on too much. I admit to loving lots of things. Most of those things are centered broadly in the arts. While being a jack of all trades though master of none often leaves me directionless, I do have invested interests in museums. I adore the study of art and have worked, albeit briefly, at a museum. After applying to galleries and other art institutions over the last two years, I have found particular hardships in my ongoing attempts to break into this facet of the art world.

Anyone who has ever applied—successfully or otherwise—to an internship or entry-level position knows that it is, to put it nicely, a process. Starting a career in any field is difficult, but it’s no secret that entering the world of museums, galleries, or foundations, can seem next to impossible. On bad days, seemingly hopeless.
 
### Let’s talk about internships and money.

If you are lucky enough to be plucked from a pile of resumes, granted a series of tedious interviews and are able to secure a job offer, the harsh reality is that a position within this field is likely not going to be a paid one. In the rare instances that you are paid, it usually comes in the form of a small stipend for lunch or travel or by low hourly rate. While some payment is better than none at all, non-graduate level internships are typically part-time positions requiring only two or three days a week of work. The only way most people could afford to participate would be to subsidize it with a flexible part-time job with above-average pay.
 
Though I don’t want to be unfair to art institutions when we exist in a country that continually cuts funding for the arts and villainizes its most daring creators, the issue of unpaid internship programs is an incredible source of worry. It’s a microcosm of larger issues within the art world and invariably hinders the future leaders of the industry. Even if you land an internship and can piece together enough money to get by for the duration of that program, the reality is that you are likely looking at several years of internship after internship until one day you have to make the decision that it’s either time for grad school or time to move on.

### Who are these internships for?

<span class="semibold-italic">Are they for undergrads?</span> I completed one of these internships while still in college and found the experience very fulfilling. I still keep in touch with one of my supervisors at the museum. However, I too experienced the money problem. The internship was unpaid and I received school credit for the hours I worked. Between the commute to and from school, acquiring appropriate office-attire, the after-hours obligations, the Upper East Side networking lunches I didn’t _have_ to attend but certainly did anyway, I nearly cleared out what little savings I did have at 20 years-old. School was always the priority, so with this internship occupying the bulk of my spare time, plans of a part-time job crumbled pretty rapidly. After one semester, I knew I couldn’t continue without seeing negative side-effects in my grades or my wallet. 

<span class="semibold-italic">Are they for grad students?</span> If an internship description asks for students seeking Master’s degrees, they definitely mean it. Museums and galleries structure these positions alongside schools specifically as a way for students to shadow curators, directors and other specialists who face the daily artistic and logistical challenges associated with heralding exhibitions and running a fine arts institution. Undergrads typically are not prepared for these rigorous internships. Is the solution, therefore, just go to grad school? Start on a curatorial track that will seemingly provide unfettered access to internships and more lucrative positions further down the line? I am an eager student by nature, so I have seriously considered this. Yet it’s also the most severe solution. Internships are meant to be learning experiences in a less academically formal and more hands-on way. They are designed for individuals to learn where, how, and, more to the point, _if_ they want to work within this field. If you are enrolled in a graduate program, you have decided your path already. Not to mention, it's _graduate school_ for God’s sake—even if you are positive that curation is the career path you want, there is no guarantee you will leave school with a job. That’s harrowing to think about. The competition, time commitment, and extraordinary tuition cost are individual factors which can hinder plans of pursuing a Master’s degree. Together they can tank those plans entirely. 

<span class="semibold-italic">Are they for recently finished undergrads?</span> This is where I stand now. Without the academic obligations of school, interning sounds a lot more doable. Being untethered to a pricey grad program is also favorable. The remaining obstacle, as I have mentioned, is that unless you supplement the internship with a very flexible and well-paying job, you are left to scramble and worry about your livelihood. 

<div class="section-break">~</div>

This whole ordeal can feel utterly hopeless at times. After studying your ass off in school, establishing what few connections you could, and researching the institutions which interest you—or, at the very least, are hiring—inevitably, you are going to find yourself choosing between establishing a career with opportunities in a world that you care about, or paying the rent.
 
A huge fear I have—besides sounding petulant—is that I am the only one experiencing this money problem. Maybe this progression I have described, which seems the obvious path to me, is not actually the best course of action. Perhaps other paths are being charted with much success. Perhaps this stagnation I feel is mine alone. But let’s say it’s not just my problem. 
 
Let us say that these internships are the way into this business. And let us say this because, besides knowing somebody, they really are. Then the real issue, and the real answer to my question is that these positions are really not made for everyone to participate in. I don’t believe in conspiracies and I don’t believe there is one at work here. I don’t think that these institutions want to keep out the poor or middle class from their ranks. Museums and art non-profits are generally populated by well-intentioned folk with at least some appreciation for the public service they provide. Unconsciously or not, though, the fact is that most of these roles go to the wealthiest among us. And, because you cannot talk about class or wealth without talking about race,

<h3 class="right-aligned-emphasis">the white among us.</h3>

<div class="section-break">~</div>

Studies concerning diversity within museums in the US show that minority employees are consistently making up a very small percentage of total staffs. For example, In 2016, New York City’s Department of Cultural Affairs [released a survey](https://sr.ithaka.org/publications/diversity-in-the-new-york-city-department-of-cultural-affairs-community/) conducted across the city’s cultural, nonprofit organizations. It concluded that nearly 62% of organization staffs are white. When seperated by seniority, the lowest level staff consisted of 55% white and increased to 74% in the senior-level. The Andrew W. Mellon Foundation [has published similar findings](https://mellon.org/media/filer_public/ba/99/ba99e53a-48d5-4038-80e1-66f9ba1c020e/awmf_museum_diversity_report_aamd_7-28-15.pdf) which revealed that within American museums, about 28% of all positions were held by non-white individuals. That same study also revealed that positions associated with the educational and intellectual mission of their institution were held by 84% white individuals.

While studies like these and the numbers they provide validate the myriad concerns over lack of diversity at all levels, several organizations have begun correcting this imbalance at the entry-level. Executive Director of the Association of Art Museum Directors (AAMD), Christine Anagnos, came up in news relating to this issue last summer. Her organization, which includes over 200 museums across the country, [announced an initiative](https://news.artnet.com/art-world/paid-college-internship-aamd-1316771) which will sponsor participating member museums to hire interns in every department, giving precedence to undergraduate students of a minority or disadvantaged background. This initiative also includes significant compensation for these roles of over $6,000 per student for the semester (when broken down, approximately $16/hour for a three day work week and program lasting four months). 
 
Other organizations of varying size are responding with similar programs. One example is Souls Grown Deep—centered in the Southeastern region of the country, SGD works to promote the work and legacy of Black artists from the region. Like the AAMD, the organization [announced just this past Fall](https://news.artnet.com/art-world/souls-grown-deep-foundation-new-internship-1346822) an initiative that will grant participating museums funds to pay a stipend of approximately $5,000 to people of color who have been accepted to their internship programs.
 
Reporting on the issues of unpaid internships and their larger implications has increased greatly in the last couple of years. As these reports shine light on implicit gatekeeping being conducted at the very base level of museums, institutions move to increase their accessibility. These initiatives I mention are a step in the right direction and it will be interesting to follow how they unfold in the coming years. And though what interns are being paid is by no means a wholly livable wage, it certainly eases some of the financial burden these roles can create. With this in mind, I suspect much more work will have to be done before I personally stop worrying about it. Unpaid internships present legitimate concerns to museums and the people who work for them. Yet those concerns are accompanied by even more serious ones. 

<div class="section-break">~</div>
 
As I sit in this stew of post-grad uncertainty, I willingly admit I am frustrated with my own inability to break into the world of museums. Since finishing school and moving to New York City, I have done my best to visit openings and attend lectures. Sometimes events are at the same institutions where I have applied to a position with no answer. More often, they are at places I haven’t even bothered to apply when I saw the word “unpaid” in the job description. For better or worse I believe in what art institutions are capable of doing for the cultural landscape. And for better or worse, I want to be a part of that landscape in some way. So I will continue to apply even when I don’t know if it’s worth it and hope that somehow I can make it work. I don’t know if there is a solution to the money problem or if I will ever stop worrying about it. Seeing as I break into a cold sweat pretty easily these days, it’s not likely to stop anytime soon. 


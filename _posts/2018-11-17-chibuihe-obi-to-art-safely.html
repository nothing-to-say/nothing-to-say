---
layout: post
title: "to art safely"
author: "chibụìhè obi"
author_slug: "chibuihe-obi"
date: 2018-11-17
permalink: /chibuihe-obi/to-art-safely.html
external_thumbnail_description: nigerian writer and vocal advocate for queer art in africa chibụìhè obi discusses why the creative act is more important than ever in the face of adversity, and why we must care for ourselves too
---

<script>
 MIN_FERMATAS = 4;
 MAX_FERMATAS = 15;
 MIN_LINES = 100;
</script>

<style>
 section {
     text-align: justify;
 }
 section > p:first-child:first-letter {
     font-size: 50px;
 }
 section > p:first-child:first-line {
     font-weight: bold;
     line-height: 25px;
 }
 .section-break {
     margin-top: 5em;
     margin-bottom: 5em;
     text-align: center;
 }
</style>

<div class="section-break">//</div>
<section>

    <p>
        We don’t always go to art. Sometimes it is the other way round: Art stirs from its little corner of the wood, dons its construction site helmet and rubber rain-boots, wades through the dripping thickets and muddy puddles, scaling barb wired fences, kicking down doors from their rusty hinges in search of us. Sometimes art comes after us/ art comes for us/ art comes to us. I know this from waiting three days in a dingy little room with my hands tied to my back and the smell of Harpic toilet cleaning liquid stinging my nose. I know this from insisting that my eyes remained shut for hours despite the quiet creaking of dawn, until I felt the rushing colours of sunlight fording across the thin flesh of my eyelids, reaching my retinas first as flamingo-pink lights, then as moonlight-yellow beams… The whorl of light flashing pictures of the last things I had seen before my captors nabbed me—objects from the other side of the world where freedom is resident. There was the metal skewer burnished black by years and years of flames leaping against it; there was the carefully arranged kebab meat sizzling and roasting on top of it. There was a lamp pole a few hundred metres away with the flurry of a thousand moths moving up and down and around it like an abundance of confetti, like a sudden, heavy drizzle of brown snow… These were the images of my last twenty minutes in freedom land that registered and arrived in waves at every dawn. And in that dingy room with the stinging smell of Harpic toilet cleaning liquid I imagined what these images would look like when rendered in acrylic, or watercolour, or blood mixed with mud mixed with crushed henna mixed with three drops of salt water from a misfit’s raised middle finger. I imagined myself painting these images across the room, bending the smokes and the flames until they snaked close enough to warm my curling toes. Perhaps I’d have given the kebab a lusty gloss of brown, like freshly lacquered wood so that whoever saw them would mistake them for an off-the-oven kebab and salivate and stuff them into their own mouths, badly tended moustache or fiery red lipstick notwithstanding. Every morning, for those three days, dawn-light threw these images across the wall of my inner eyes, and the possibility of them metamorphosing into real, tactile art stood between me and total despair. The thought of turning memory into images and further into art warmed my heart, pushing anguish and the hopelessness of my present condition aside. There was the hope that, somehow, I might make art that kindled a flame, art that occupied the numbing emptiness, art that made the tongue hunger for kebab and freedom. Synaesthesia somehow is art coming to us in the form of hunger or thirst or memory. Daybreak too is art coming to us as awakening, as awareness, as a quiet and sensuous resurrection. Light travels faster than sound… I’m telling you, art rides shotgun on that spaceship every morning to get to us.
    </p>
    <div class="section-break">//</div>
</section>
<section>
    <p>
        Art saved my life.
    </p>
    <p>
        There are more than a hundred results that pop up when you enter that into Google’s search engine. <a href="https://youtu.be/eMQ_FCY-OgE">In one video</a>, Jonathan Wynn-Strachan talks about songs and homelessness and an absentee father and his community project: 60 Minutes of Art. <i>…I was able to put smiles on twenty five faces with paper, markers, crayons and our imagination</i>, he said at the end of the six minute talk. At once, art dangles in your face as a multi-coloured candy, candid joy, attractive, yet deep, healing, enduring.
    </p>
    <p>
        In Italy, a Nigerian artist who makes sculptures of warriors and legends from folklores <a href="https://migramundo.com/my-art-saved-my-life-says-nigerian-artist-after-crossing-mediterranean-sea/">talks about tragedies and escapes and survival</a>. He has braved the tumultuous journey from North Africa to Italy through the Mediterranean, forging ahead in his resolve to survive, despite the waves and the menacing reminders of death that swam up sometimes in the form of abandoned orange coloured life jackets. In 2015 alone, the year he made that journey, an estimated 3,771 migrants died. And in the 2015/2016 period, the number of deaths and missing overcomes six thousand… <i>Despite my desperate journey, my art saved my life</i>, he said while mixing and moulding clay, fibreglass and cement into form.
    </p>
    <p>
        (To make art in the face of total opposition, oppression and violence is an overwhelming task. People are leaving Nigeria. Artists are running away. Some prefer the scalding saltwater of the ocean to fading into dust or ash)
    </p>
    <p>
        An old man in plaid shirts, who has just been released from jail, squints into the camera, his eyelids heavy and pressing down on his face like sandbags. He speaks to an invisible microphone. His voice trickles down from the television slapped against the wall. <i>…art saved me</i>, he whispers solemnly again and again, as two of his grandchildren stare curiously at him. <i>Art saved me</i>, as he weaves his step across the cobbled walkway of the neighbourhood, as he eases himself through the doorway, his hair wispy and nearing grey, the rancid air of the jailhouse and thoughts of suicide finally behind him.
    </p>
    <p>
        On a hot windy afternoon in 1968—25 years before I was born—my mother was sprawled on a dusty patch in a refugee camp, cradling a thin slab of wood and a dog eared copy of Oliver Twist, trying to paint on the dark wood with the help of a few broken pieces of chalk, a picture that reflected exactly what was on her mind. My mother, a scrawny faced teenager, fifteen, maybe. Eleven, twelve, thirteen, fourteen, even. My mother, in a dusty yellow dress with flecks of dust in her hair, cradling pieces of broken chalk in her palms, balancing a sludge of thoughts, a chaos of images in her head. My mother, suddenly oblivious of her former life, wholly taken by a gentle wave of thought that, if indulged, could transmute into art. My mother dreaming of filigrees or indistinct figurines, dull illustrations, images, intuitive designs… A bomb raid had gutted their house and everything they once owned and called home had gone up in flames. Even the memories of her birthday had caught the flames, becoming fumes of black smokes, scanty recalls, horror, dark noise, nothing.
    </p>
    <p>
        As a scrawny faced teenager in a refugee camp with trauma where the memory of her age and birthday should be, she sat before the thin slab of dark wood, caught in the throes of art making. To engage in art making in such a desolate environment, with the limbs of survivors still held up in casts and the faint hum of war ricocheting in the distance speaks more of an abiding faith in the power of art to take one through the rough roads of adversity than mere hobby or habit can explain.
    </p>
    <div class="section-break">//</div>
</section>
<section>
    <p>
        The above collage from real-life situations is an attempt to mount this motion on an easel. I am summoning all these stories, these histories to stand as witnesses. When the human body shipwrecks in a disaster, the mind runs to art. In moments of angst, it returns to art.  In solitary confinement, poets take up letter writing, exploring the disturbed geography of their thoughts by way of copious cursives. Painters throw their own shadows against the high walls of their cells. Composers hum stray tunes over and over and over until they filter out as wind chimes. Prophets dream, sink into rapturous reveries.
    </p>
    <p>
        After the Nov. 13, 2015, Paris attack, graphic memoirs and novels, and other forms of art are beginning to emerge from the survivors. Art as a coping mechanism is the stepsister of art therapy. From sanatoriums to asylums to refugee camps to cancer wards and safe shelters, art sidestepped its traditional use as a medium for communicating feelings, thoughts and ideas to a means of understanding one’s inner feelings and grappling with them in periods of immense distress.
    </p>
    <p>
        After witnessing the lynching of a gay man, I ran toward my notepads to write. Although I couldn’t poem or sketch, I still held unto the soft mass of the notepad, as if the weight of the collected pages was an anchor and my body a floundering ship.  Art making might not be the escape route when terror bares its teeth. But we can call it a placeholder and cling unto it. We can call it salvation and surrender.
    </p>
    <p>
        Chris Abani, the US based Nigerian poet and writer recalls during a book conversation a momentous encounter in jail, a brief but abiding brush he had with the legendary Afrobeat musician, Fela Anikulapo-Kuti. Both the man and his musical instrument; the legend and his saxophone. They’d both been imprisoned at the same time, in the same prison, by the same regime. Despite the fact that the circumstances surrounding their arrest were unjust and the experiences gruesome (like when he saw one of the inmates’ penis being nailed to a table) he managed to cling unto the joy of watching the legendary artist tickle laughter out of the saxophone. And that experience impacted him, transformed him. Today, outside the sturdy walls of the prison, he plays the saxophone as well as poeming, prosing, photographing. I’d like to think that when his freedom was snatched by the keepers of the gulag, he held unto the glitz and sexiness of the saxophone.
    </p>
    <p>
        Art as anchor, as placeholder.
    </p>
    <div class="section-break">//</div>
</section>
<section>
    <p>
        Art saves.
    </p>
    <p>
        It is true.
    </p>
    <p>
        But what happens when the same art demands sacrifice? What should the artist do when their work asks for payment that is not just in salt and sweat, but with their own blood?
    </p>
    <p>
        In scrabble, there are 162 possible four letter words you can make with adversity. <i>Arts</i> is one of them. All art is either made or conjured through some kind of exertion, most from the crucible of anguish. From adversity, art emerges. The artist, a bleeding mess—if they survive at all.
    </p>
    <p>
        I was abducted for exploring queer identity in my writing. Shortly after I was released, my mother called me on the phone. It was minutes after midnight. Quite strange. She doesn’t know me as a writer. I am her last child.
    </p>
    <p>
        <i>Promise not to write those things that might infuriate them</i>, she said.
    </p>
    <p>
        <i>Promise not to put your life on the line again.</i>
    </p>
    <p>
        <i>They could kill you.</i>
    </p>
    <p>
        <i>Promise not to write.</i>
    </p>
    <p>
        <i>It is dangerous here.</i>
    </p>
    <p>
        <i>There are other things…</i>
    </p>
    <p>
        <i>Promise not to risk it.</i>
    </p>
    <p>
        <i>They might come for you again.</i>
    </p>
    <div class="section-break">//</div>
</section>
<section>
    <p>
        I am trying to make art. I am trying to survive. I am trying to rage, to ball my fist, to shake loose the dust of oppression. It is a wet edge, a crucifix, a suffocating confine. There are no back doors, not even a window. Everything is a chaos of heartbeat and twitching nerves. The space is too small, it pinches. The dust is everywhere, twirling thick. I cannot breathe. But I need to… We need to…
    </p>
    <p>
        James Baldwin wrote in his autobiographical note: “I consider that I have many responsibilities, but none greater than this: to last… and get my work done”.
    </p>
    <p>
        I want to last. So I pick up my shoes. And run.
    </p>
    <p>
        Three queer writers were faced with the imminent dangers of abduction, death, or both. They picked up their shoes and ran.
    </p>
    <p>
        From three different frontiers, they ran, headed for a safe shelter.
    </p>
    <p>
        Whatever genre of writing, whatever medium you use to protest oppression, whatever form your rage assumes, one thing is unmistakable: you must be alive to do and continue your work. As an artist, one of our moral duties to our art is to stay alive and bring it forth. Scrawling with trembling hands, throwing the paint brush with thick blood running down your face—the situations are usually desperate, urgent—but the heart must maintain its beat. The rhythm might be out of sync, desperate as desperate can be, but the heart must continue to beat. The work of the artist is not either protest or self-preservation. It is both. We scream and also do self-care. We rage and also rest. And run sometimes. We howl and hide sometimes. This is how to survive terror.
    </p>
    <p>
        Ernest Hemingway writing to his old friend F. Scott Fitzgerald said, “Forget your personal tragedy. We are all bitched from the start and you especially have to hurt like hell before you can write seriously. But when you get the damned hurt use it—don’t cheat with it. Be as faithful to it as a scientist—but don’t think anything is of any importance because it happens to you or anyone belonging to you… You see, Bo, you’re not a tragic character. Neither am I. All we are is writers and what we should do is write.”
    </p>
    <p>
        Whether we are dodging bullets, escaping a mob, or experiencing some other personal trauma, we must not forget that what makes us the artists we are is the work we put out. Not the trauma.
    </p>
    <p>
        Art, especially in times of adversity, can be a two edged sword, offering inner respite on the one hand and also exposing us to various forms of physical harm.
    </p>
    <p>
        Art as both saviour and an unwitting scarifier. Art as both supple hands and glistening blade.
    </p>
    <p>
        But the ultimate goal is to write, sing, do language.
    </p>
    <p>
        To art safely.
    </p>
</section>

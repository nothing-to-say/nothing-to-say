---
layout: post
title: "getting art, and getting out of the way"
author: "jonah udall"
author_slug: "jonah-udall"
date: 2018-10-13
permalink: /jonah-udall/getting-art.html
external_thumbnail_description: jonah udall challenges us to stop telling people how to experience things
---

<p>
    <span class="paragraph-opening-splash">Art today has a problem. People don’t <i>get</i> it.</span> Not only that, they’re often uninterested in <i>getting</i> it. The problem is wider than any medium or style—the public largely feels there is something about much art that is inaccessible.
</p>
<p>
    If we’re not ignoring the problem, there are two places to look to address it: in how art is presented/received, and in the art itself. Many artists, scholars, critics and institutions take the problem straight on. If people don’t <i>get</i> it, let’s interpret it for them. This results in art appreciation courses, critical analyses in the press, and program notes/museum plaques that describe what to hear and see. All designed to equip the public with enough words and concepts to receive art intellectually. To <i>get</i> it. The interpretation approach also manifests in art itself—for example, in abstract works with specific subject matter that require explanation.
</p>
<p>
    Art interpretation creates healthy dialog among artists, and beyond across other fields. But if art were made only to be interpreted it would be insular and self-referential, or at best redundant. There is another approach, which has created and presented some of the greatest art of the last century, but that has failed to become our cultural norm. I am writing to give it a little more support.
</p>
<div class="vignette">
    <p>
        I’m standing on the balcony of Davies Symphony Hall with my father and a close friend, during a Steve Reich retrospective. My father described the meditative state the music brought him to. My friend saw a kaleidoscoping array of colors. I was gripped by the momentum of its relentless, subtle changes. Which one of us <i>got</i> it?
    </p>
</div>
<p>
    We can reframe the problem entirely:
</p>
<h4 style="text-align: right;">
    We don’t <i>get</i> art; art <i>gets</i> us.
</h4>
<p>
    Art has power largely because it invites experiences. Consider what brought you to your creative path; we often tell these love stories in terms of feelings, or with physical analogies like being “hit” or “flooded” or “transported.” By talking about ourselves, our own experience. One doesn’t <i>get</i> an experience—one <i>has</i> an experience. A unique, personal possession at one’s momentary intersection with a work of art.
</p>
<p>
    This is not a new idea. In <a href="http://www.afiavimagazine.com/music-and-religion-in-africa/">traditional ecstatic ceremonies</a> across the Gulf of Benin in West Africa, drumming is the catalyst for possession by a spirit—a personal experience of the highest intensity. This takes a collective form in African-American <a href="https://en.wikipedia.org/wiki/Ring_shout">ring shouts</a> (created by the descendents of those same West African peoples) and in <a href="https://economictimes.indiatimes.com/sufi-music-the-song-of-the-soul/articleshow/7840662.cms">Sufism</a>, where music and dance can induce spiritual ecstasy.
</p>
<p>
    But art has served many functions throughout history. It has served social functions too, like <a href="https://www.metmuseum.org/toah/hd/plai/hd_plai.htm">recounting and celebrating the life of the deceased</a> in the Blackfoot tribe, or <a href="https://www.guggenheim.org/arts-curriculum/topic/art-and-ideology">imparting government-approved Values</a> in Soviet Russia. In most corners of the globe, it has depicted the Divine. In Industrial Europe, art sought to express Truths about Humankind.
</p>
<p>
    However, in the last century, globalization has challenged such Absolute aims in every culture it has touched, turning us instead towards humanity’s rich plurality. International migration and communication create intersectional identities that produce a greater diversity of perspectives everywhere. Previously invisible peoples and minorities claim voices in countless struggles for sovereignty and civil rights around the world. An important task of our time is validating these differences. Celebrating diversity at the level of personal experience.
</p>

<h2>
    writing about art;<br>dancing about architecture
</h2>
<p>
    So, what does this approach require in art? First, to stop making audiences feel that they need to <i>get</i> it in the first place.
</p>
<div class="vignette">
    <p>
        I’m in a theater in downtown Brooklyn for a performance by LEIMAY, a Williamsburg-based dance company that has been making visceral multi-media work and cultivating community in their warehouse space since long before it was cool. Bodies and voices erupted in singular gestures—vocalization and movement blurred in personal offerings from each of the six performers. In the Q&A afterwards, words like “interesting” came up along with puzzled questions about the meaning of the nonsense syllables. But some were deeply moved, and described the intimate communion with these artists that the work invited.
    </p>
</div>
<p>
    Art as experience requires suspending the effort to understand intellectually. This left-brained activity tries desperately to fragment a work into digestible bits and jam it back together in a familiar form, cutting off the right brain which is fully capable of experiencing the work exactly as it is.
</p>
<p>
    Concert programs, museum plaques, articles and radio programs about art—even the artist’s own words—can imply that “understanding” the work is more important than the experiencing it. By giving interpretations, we indicate that expertise is needed in order to understand, or even to know what is good.
</p>
<p>
    Instead, let’s use these words to create invitations. To leave questions with no answer. To suggest different ways of looking or listening, from which to forge a personal relationship. To validate all experiences, even the uncomfortable ones. To allow audiences to feel okay with not knowing, so they may surrender to the experience. I don’t care if someone can tell me what they heard or saw, I just want to know what it was like to witness it—if it took them somewhere.
</p>
<p>
    There is a popular saying that ‘writing about music’ (painting, etc) ‘is like dancing about architecture.’ I’ve often heard this cited to dismiss writing about art altogether. But ask a choreographer, architecture can be a rich subject matter for dance. Writing about art should be like this, an artistic practice itself—a far cry from the scientific dissections we so often see.
</p>
<h2>
    expression
</h2>
<p>
    There is a more subtle barrier to experience in the language we use when discussing art—that of expression.
</p>
<p>
    The problem with expression as a concept is that it presupposes a code. Expression uses symbology to point with direct correspondence to something, like a word to an object, or a <a href="https://greatergood.berkeley.edu/article/item/are_facial_expressions_universal">facial expression to an emotion</a>. And if something is being expressed, the question is ‘do I understand the code?’—‘do I <i>get</i> it?’ In art, this creates the illusion of a language barrier—that there is something external that needs to be learned or understood. A barrier to direct experience. (Historically, some art has had such codes. Take Romantic leitmotifs, Renaissance symbolism, or the movement vocabulary of Classical ballet. A different function, for a different time.)
</p>
<p>
    Instead of a language, I think about art as a doorway. A doorway opens through a wall into a new space. An effective doorway is empty; nothing is in the way. The artist builds the door, and the audience opens it.
</p>
<p>
    Artists often talk about self-expression. I think it’s quite limiting to say that a work expresses “yourself”—I hope we all believe we contain much more than any single work could capture. If you say it expresses a part of yourself, has something been withheld? Regardless, we are not the only side in the exchange between artist and audience, and the prevailing imbalanced attention on us may actually compromise access to our work, not enhance it. We ought to stop taking ourselves so seriously; art is too serious for that.
</p>
<h2>
    presenting invitations
</h2>
<p>
    We have a responsibility in the creative act itself to nurture experiences. I can’t tell you exactly what this means for your own practice, but I can offer some ideas to consider.
</p>
<ol>
    <li>
        Assume the audience is smart. Acutely perceptive. Most audiences will not have the technical (read: interpretive) perception artists do, but they certainly have the same human capacity to respond to experience. It’s a self-fulfilling prophecy; if you think they can respond to it, you will make it so.<sup id="footnote-ref-1"><a href="#footnote-1">1</a></sup>
    </li>
    <li>
        Make art that is intended to be seen in different ways. We contribute to the illusion of a language barrier by creating work with a fixed interpretation in mind. Don’t take your own perspective so seriously.
    </li>
    <li>
        Think about what you do as presenting an invitation. Creating a doorway. There are as many ways to do so as there are human beings. See, for example, Rothko creating a container for reflection, Cage bringing attention to sounds on their own terms, Cortazar subverting reality just enough to invite questioning the solidity of things, Oliveros asking audiences to vocalize their listening, Bach layering melodies with infinite listening pathways between them, or Bausch using visceral movement to open the door to the subconscious.
    </li>
</ol>
<h2>
    the human behind the door
</h2>
<div class="vignette">
    <p>
        I have spent many hours in the Flatiron’s fifth floor listening room, the Jazz Gallery, but the dim red lighting on the stage where Matana Roberts stood created a space I hardly recognized. (Critical convention would have me qualitatively assess the ambience and connect it thematically to the performance. Perhaps useful for a performer interested in how lighting can support their work, but this is the kind of left-brained fragmentation that takes us out of experience.) For an hour, saxophone melodies and spoken stories interrupted each other spontaneously as she bore her consciousness unadulterated. Even the façade of medium was torn down, along with any pretension about Art or Music. The art itself was invisible, placing nothing between us and her process of discovery.
    </p>
</div>
<p>
    We don’t need to “express” ourselves to be understood, we only need to open the door. It’s not what we create, it’s how we get out the way.
</p>
<div class="footnotes">
    <ol>
        <li id="footnote-1">
            <a href="#footnote-ref-1">These ideas come from choreographer/theorist Mary Overlie in her book "Standing in Space" (2016)</a>
        </li>
    </ol>
</div>

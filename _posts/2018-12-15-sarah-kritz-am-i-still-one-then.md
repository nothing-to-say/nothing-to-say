---
layout: post
title: "am I still a dancer?"
author: "sarah kritz"
author_slug: "sarah-kritz"
date: 2018-12-15
permalink: /sarah-kritz/am-i-still-one-then.html
external_thumbnail_description: When is a dancer still a dancer? Sarah Kritz asks us and asks herself in this micro manifest
---

<script>
 MIN_FERMATAS = 4;
 MAX_FERMATAS = 100;
 MIN_FERMATA_SCALE = 0.01;
 MAX_FERMATA_SCALE = 0.15
</script>

Though I’ve formally studied dance for twenty years (give or take), I still don’t know if I qualify as someone who can go around calling themselves a “dancer”. Because, in my mind, when I say this, I am grouped along with members of renowned dance companies or Broadway shows or members of rigorous dance conservatories (like the one whose classes I snuck into in college) that I in no way claim to be in the same sphere as. But the terminology remains the same. It’s not like you can go around claiming you’re a “doctor” when you didn’t graduate med school, or a member of any other profession that also requires a lifetime of formal training, hard work, sacrifice, and discipline.

In college, I took as many dance classes as I could that matched my skill level, for majors and non-majors, without a traditional audition. My countless emails to the program director and various professors earned me a trial class, which I passed. On campus, when I said I was a “dancer,” I made sure to use the prefix “non-major,” so as no to equate my meager twenty hours a week of class/conditioning/stretching/rolling out/prepping to the exponentially long hours of conservatory students. Yet, I used (and still use) this identifier because I believed it to be the most true. Though I’ve studied various other visual and performing arts, the one thing I have consistently stuck with, daydreamed about, made life-altering decisions around, is dance. And still, in my visual arts classes I felt most like a dancer, and in my dance classes I felt most like a comedic/conceptual performing artist. 

Is a dancer defined by the amount of raw talent or sensibilities they have or by the amount of work they put in? The amount of physical time spent in the studio, the portion of that time spent actively focused and trying to improve? The amount of time angry that I fell off my leg, or didn’t get the combination, or zoned out completely, or dissociated because of the oozing imposter syndrome living in all of my joints or my sadness or hunger or cramps or injury or pain or a general sense of being overwhelmed? I heard Dakota Johnson experienced some sort of panicked episode of not feeling she was good enough to perform her dance scenes in _Suspiria_, and I thought _wowowowow_ no dude you _got_ it. At least in my experience, and through observing my peers over a lifetime, that is such a prevalent part of what being a dancer is. Or being a performer. Or being a human, I guess _(blah blah blah blah blah)_.

So I guess what I’m asking is if I don’t make my living being a dancer, and sometimes go months without taking a dance class—or even working out/checking in with my body at all—or even _thinking_ about dance class, or if I can be all consumed by things that have nothing to do with dance at all, am I still a dancer? If I didn’t put everything else aside to pursue dance, does any of the work I put in still qualify me to call myself a dancer? When my body fails me and to dance as I’d like to is no longer an option, am I dancer still then? 

I recall taking my sophomore year of high school off from dance due to a knee injury. This dramatic dive of endorphin intake/lack of physically creative output/no access to an activity that focused my pinball-ridden mind and always-fidgety body feels like one big cloud on my memory’s timeline. I still hadn’t made close friends at school, I wasn’t regularly seeing my dance friends, my first AP class was kicking my ass, and the echoes of puberty riddling throughout my body only further estranged its relationship to itself. This general malaise made me realize what an important part of my life dance was/is. 

I believe there will always be this siren call back to the studio; the sweat, the joy, the pain. It is the wall I constantly bounce my tennis ball self off, the warm hug of familiar history and routine, the litmus test I take to size up what I really want and who I really am. Who I am beyond words, beyond reason, beyond this plane of existence and its limits of reality.

So then, yeah, I guess. I am one.

_(cartwheels away)_

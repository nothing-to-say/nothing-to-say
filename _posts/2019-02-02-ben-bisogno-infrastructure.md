---
layout: post
title: "infrastructure for making"
author: "ben bisogno"
author_slug: "ben-bisogno"
date: 2019-02-02
permalink: /ben-bisogno/infrastructure.html
external_thumbnail_description: "From the Kansai dispatch, a brief note about some prerequisites for productivity"
---

<div class="editors-note">
<i>this article was originally published on <a href="https://pinknoise.home.blog/2019/01/31/life-and-infrastructure-here-in-kyoto/">Ben’s personal blog, ‘pink noise’</a></i>
</div>

#### FROM THE KANSAI DISPATCH---

More and more over the years I understand the crucial importance of infrastructure in creative work. 

Infrastructure first encompasses the artist’s workspace, her office or studio that allows her to just sit down and start creating.

This is as opposed to having to unpack her belongings from her bag every time and re-set-up her workflow. This can even be embodied by minutiae such as an extra power cable plugged into her office at all times; having to always duck under her desk to reconnect is a minor annoyance that, when enfolded with other barriers, forestalls the flow she needs to make great art.

Infrastructure secondly encompasses the artist’s worklife, how her commute, schedule, and the norms of her apartment make getting down to her work easier or more difficult. 

I, for example, am currently in a wonderful one-bedroom studio apartment on the west side of Kyoto city. The space has surprisingly high ceilings and laminate floors; however, for all the beauty the space brings with it, I am not allowed, by contract, to practice music—something crucial to my work as a composer. At the same time however, I chose this space because it is not even twenty feet away from my campus, where (only) from the hours of 6am to 9pm, I can practice and access the Internet. My apartment doesn’t have internet access because it would require a two-year contract, something I do not want to sign given my intention to move. 

The third sphere of infrastructure, which is crucial to creative life, is the field of one’s community and one’s internal state. It is very difficult, and I think even unhealthy, to always be your own advocate. To constantly invite folks who do not quite care to events you have put your soul into is tiring work; to not have a community that supports your endeavors is to be pulled by gravity into normativity, to be grayed by the ennui of one’s people. Moreover, to have one’s state or town provide limited or difficult access to performance, community, and presentation spaces is to confront a real, and even aggressive, opposition to the very possibility of art. Art takes investment in material funds, in public infrastructure, in outdoor spaces and affordable venues. 

I have been thinking about this all myself due to the many barriers I am confronting here in Kyoto. It’s personal. Below is something of a self-help list, reminding me of that which I still need to face in order to flow with my art.

I need:

- more light in my studio
- more accessible internet
- a keyboard, and a place to put it
- an apartment which is close to school and where I can play music and hang up paintings
- to tell my professors—directly—that I need them to support me more than they currently are
- to connect with more performers more readily, for which I need to set up a listening group and pay performers to record pieces for me
- to create a facebook page for larpers in Kansai and host meet-up events
- to create a podcast called Kansai Creatives that interview the works of creators of all stripes here in the local area

Kyoto is backwater in many ways, but it does have a strong art scene, hidden behind the teary concrete walls of the city. And, given real access to the internet, I could actually be connected to the greater community in what I sometimes feel like is the “real world,” that is---for me---New York, Chicago, Boston, and Berlin. Right now, however, I have the chronic sensation that I’m swimming in a fluid fractured bit of reality, artistically ungrounded by the infrastructure I really need to do my work.

_---Ben Bisogno, February 2, 2019_

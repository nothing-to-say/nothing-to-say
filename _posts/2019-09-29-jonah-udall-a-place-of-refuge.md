---
layout: post
title: "A Place of Refuge"
author: "jonah udall"
author_slug: "jonah-udall"
date: 2019-09-29
permalink: /jonah-udall/a-place-of-refuge
external_thumbnail_description: "Jonah Udall asks, &quot;How can I make art when people are dying out there?&quot;"
description: "How can I make art when people are dying out there?"
---

<style>
  .post-content > ol > li {
      margin-top: 1em;
	  margin-bottom: 1em;
  }
  .opening-question {
      font-style: italic;
	  text-align: right;
      margin-top: 2em;
      margin-bottom: 4em;
  }
</style>

<h4 class="opening-question">How can I make art when people are fucking dying out there?</h4>

In quiet moments, when this question arises, I often feel small. Helpless, maybe. I live with a conviction deep in my body that I _must_ create, but my mind righteously shouts, “What good is that doing for the injustices of the world? You’re squandering your birth-given faculties (and the privilege of a straight, white, cis-man) by narcissistically whacking on a box with wires. Great choice.” It’s shame speaking, who always knows the quickest road to paralysis.

Wiser minds than my own have expounded profusely on the value of art to Humankind---as a universal language, an uplifting force, a spark of revolutions, a fundamental expression of human nature. Like many artists, I have taken solace in their words. But this question inevitably finds its way back to nag my conscience. The problems we face are _too real_---climate injustice, systemic discrimination, a broken legal system, voter suppression, the list goes on. I should be _doing_ something.

I’m writing to propose that, by making art, we _can_ do something. That the power of art to change the world isn’t to be merely taken on faith, but acted upon. Not as a righteous crusade, but rather right in your own heart.

I offer the following reflections on the role art might play in the fight for Justice. I urge you to read this not as a self-congratulatory pat on the back for artist-hood, but as an invocation to look more closely at this position, and treat it with its due importance.

1. *Art makes sacred spaces.* Spaces that reveal to us the Divine, the Great Mystery. The predominance of spiritual art across human history speaks for itself. It’s Art that makes the Temple divine. Take the Art out of the Temple, and the new space it finds itself is, too, sacred. Even unexpected places: Turn into an alleyway in San Francisco’s Mission District and, engulfed by towering murals, you’ve stepped onto hallowed ground. A New York subway car disrupted by a skilled aerialist is consecrated as a Temple of the Divine. The space itself changes; the choice is whether to take the invitation. To place the onus of transformation on the artist-audience axis is an unfair burden to all. Our responsibility is to transform the space. This potentiality is tremendous.
2. *A sacred space offers refuge.* Refuge is offered, not administered; it must be taken wholeheartedly. This is far more powerful than escape, as some suggest art to be. Escapism pushes away our pain and undermines personal strength. Refuge is fortifying, a secure place from which to enter fully into our lives, the problems we face, the world around us, with renewed strength and a wider view. As Zen teacher Rev. Kosen Greg Snyder says, refuge is the first step of transformation. We have all seen altruistic conviction end in burnout or conflict without a foundation in refuge. We cannot heal the world from a place of hurt, and we cannot help but heal the world from a place of love. The aircraft safety protocol urges us, put on your own air mask before assisting other passengers. Refuge is oxygen for the heart.
3. *Refuge gives space for emotion.* We artists need not worry about conveying feelings of our own or, more futilely, eliciting specific emotions from the audience. It is profoundly radical simply to permit the Emotion of our lives to move through us, artist and audience. How rare an offer that is---the space to open beyond habituated patterns of shame, overwhelm or numbness. Catharsis is a powerful antidote to paralysis.
4. *Refuge asks us to look below the surface.* With the modest time and resources I have to give, there are thousands of worthy organizations to support with an equal number of answers to an unending plethora of problems. It is natural for the activist to be [paralyzed by choice](https://www.youtube.com/watch?v=VO6XEQIsCoM). Refuge invites another response. In a sacred space we see the divine. In the divine, we see ourselves. Here, we can touch our most essential values. This is a far cry from moralism; values are reflected, not projected.
5. *Art (and refuge) invites play.* For the creator, of course. But I think the play of the observer is equal---of attention, emotion, perspective---the play of refuge. My opening query addresses the first of our three famously inalienable rights---life, liberty and the pursuit of happiness. It is easier to forget the importance of the last one. Architect Jonathan Hale writes in _The Old Way of Seeing:_

    > “Jefferson gave play its proper place when he made happiness one of the three highest principles of democracy. He was, after all, an architect. Jefferson derived the rights of the Declaration from John Locke’s “life, liberty and property” … But property, Jefferson saw, has nothing to do with fundamental principle; property is in service of life, which must play. Our Declaration at its simple deepest makes the national goal the protection, not of our livelihood but of our liveliness.”

The responsibility of refuge is profound. Refuge is founded on a contract of trust that we artists need to earn, and honor. Is the space we are creating safe(r)? Are we perpetuating narratives of oppression? Regardless of the motivation (or lack thereof), acting them out is a deep violation of the contract of refuge.

It starts with making sacred spaces. Let’s stop sanctifying places of privilege, or reinforcing privilege in the places we sanctify---concert halls, galleries, theaters. The power of refuge is profound. Where are we wielding it, and for whom?

It is crucial to acknowledge that refuge does not look the same way for all people. A refuge is an invitation that will not be equally welcoming to everyone. We must also consider _what_ we are offering as refuge, _how_ it is offered, and _who_ we are as the offerer. These questions are meaningfully addressed when artists converge with community, itself a powerful source of refuge.

The gravity offering refuge needn’t drag us to inaction---the seriousness of this endeavor is held by the joyfulness of play. Let’s take our work to the streets, start conversations, and consecrate holy ground as only art can.

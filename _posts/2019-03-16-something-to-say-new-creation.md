---
layout: review
title: "New Creation: gender-fluid goddesses, amazon, and the play to end the world"
date: 2019-03-16
permalink: /reviews/new-creation
external_thumbnail_description: "In the beginning, there were four gender-fluid goddesses... at least in the universe of New Creation, Kai-Chieh Tu’s latest work of experimental theater, premiered at Dixon Place on February 28th."
---

In the beginning, there were four gender-fluid goddesses... at least in the universe of New Creation, Kai-Chieh Tu’s latest work of experimental theater, premiered at Dixon Place on February 28th. The show featured performances by Katherine Wright, Janice Amaya, Laura Yumi Snell, Sharlee Taylor, and Mark Hayes, music by Sugar Vendil and Eric Umble, a set by Chen Wei Liao, lighting by Rob Lariviere, costumes by Wang Ying Tu, additional media by Giada Sun, and was managed by Rachel Oshrin and Taylor Hunsberger. 
Four goddesses must put on a play, heralding the end of humanity. This play has to be “equally engaging, shocking, repulsive, entertaining and accessible to the whopping 60 billion spectators.” To prepare, the goddesses immerse themselves in the world of contemporary humanity: smartphones and social media, fast food and Amazon, tools and luxuries, psychedelics and cocaine. The exposition opens up into a space of exploration, where the effects of each new object are explored for each personality and relationship. 

What began as a collective group of god-beings fractures, as cliques form and relationships strain. Material items, even something as banal as a toilet, become objects of unhealthy fixation. The gods’ attention becomes focused on status, social perception, and instant gratification. Relationship dynamics are strained through group chats. As they run the treadmill of materialism, the stage fills with _stuff_, with each item given only a moment’s thought before moving on to the next. 

The play is punctuated by appearances by resurrected celebrities. Shakespeare is summoned to help write the play. Centuries of endless lauding transformed him into a demon of narcissism, a 6-inch doll dominating the stage with the weight of his own self worth. His monologue has a sense of familiarity; Shakespeare is every old dead white man, institutionally embedded in our cultural consciousness, perpetually worshipped by wannabe academics and other old almost-dead white men.

After exploring the trappings of the 21st century and experiencing a drug-induced change of consciousness, the exasperated deities try to understand why humanity is the way it is, and how it got this way. In their desperation, they resurrect Hitler, who takes the form of a naked Ken doll. Hitler speaks only in quotes that are not his own, preferring instead to reference American Founding Fathers. Stripped of the symbols of his individuality, Hitler represents humanity. 

Discouraged by the discovery of the simultaneous beauty and fucked-ness of humans, the angels tear down the stage, the set, the costumes. The final scene of the piece is an extended lecture, tying together themes, loose plot ends, and any and all social-political ideas the author felt necessary to include. The actors pleaded and implored, to DO something, to take some kind of action, to respond to the implicit and explicit messages of the play somehow in the world at large.

I felt my engagement drop like a rock. I looked around at the audience. This message, stated with such _importance_, felt wasted on the small group of a couple dozen in attendance. Who in this group would disagree? To whom would these ideas be new, or controversial? Of these few dozen people, does anyone here really need convincing? 

It is worth noting, however, that when the social/moral/political messages weren’t simply stated plainly in a monologue, they were entertaining and facilitated the plot well. The initial exploration of the world of humanity was a perfect blend of honesty and sarcasm. The implicit commentary in the selection of objects and their effects on the characters was poignant and entertaining. 

In addition, the space created for the play was very engaging and creative. The set largely consisted of found objects, some made into imaginary heavenly devices that existed in the space of the gods, and some simply existed as they were, used to explore the human world. Part of the set was a projection, overlaying text that at times provided context, and other times would contradict spoken lines in the play.  A mysterious figure in a hard hat and overalls, the only male in the performance, rearranged the set during transitions but received the treatment of a character, subservient to the goddesses but independent of their personal goals. The sound was a combination of pre-recorded backing tracks with live performed piano and sound effects. 

The question begs to be asked, why was the lecture necessary at all? Was it anything more than a plot device, for the sake of quickly shooting through social commentary and advancing the narrative?

Despite the monologue, the play was entertaining, cohesive, witty, contemporary, and flowed at an unflinchingly rapid pace. As a whole, it was an experience well-worthy of a night out, and I look forward to seeing future work from the creators, and to see how their styles evolves.

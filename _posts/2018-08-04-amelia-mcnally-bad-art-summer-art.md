---
layout: post
title: "Bad Art, Summer Art, Hallowed be Thy Name"
author: "amelia mcnally"
author_slug: "amelia-mcnally"
date: 2018-08-04
permalink: /amelia-mcnally/bad-art-summer-art.html
external_thumbnail_description: an ode to art without air conditioning
---

Give me the art of the summer, the stuff produced quickly and profitably and consumed by stickiness and sweat, by glistening skin and short tempers, low-cut shirts and exposed legs.  I want art that pairs well with cotton candy and ice cream cones and copious amounts of alcohol.  Cheap street art no more than paint scraped over canvas to give the illusion of clouds.  Gawdy artisan jewelry suddenly irresistible in shop windows.  A man playing Clair de Lune on accordion on the boardwalk.

I want the endless pictures of mountain summits, lush and green after an afternoon hike, of red-glow sunsets over the ocean, of the blurred lights of Coney Island after dark.  We are all artists for these months.  We all can spew a monologued critique of the Blockbuster films we watch back to back only for the escape of the dark, air conditioned theaters; we all learn to dance to the beat of that one pop song, distant and ever-present wherever we go, the one we are well tired of by August and yet still cannot resist swaying our hips; we all notice that extra sparkle in the concrete, the waved patterns of distorted air peering into the distance, the elegant drip of condensation down the sides of our drinks.  We all know how to glamourize the steady singeing of our atmosphere because we all revel in the way it helps our skin to burn.

Give me mornings when the ground is still warm from yesterday’s sun and my coffee is too strong and too sweet, and I can sit with my toes curled in half-dead grass as the air churns uncomfortable.  I’ll bathe in this bad art with my breakfast.

Bury me in the heat.  Drown me in humidity, peel the skin from my shoulders and replace it with pre-cancerous cells.  Turn my blood to soup and leave me to stew in the eternity of the hours so that I must watch the world pass within its forced laziness: animals sprawled in prized shade, green plants slouched down to greet the ground, people melting slowly into the fabric of their armchairs.

These are not beings looking for deep-thinking.  We do not want complex analogies of what it is to live.  We want to feel our blood rush, to smile and laugh and forget and sweat and dance and dissolve and breathe long and hard and pretend that we never have to exude effort ever again and that all this is forever.

So give me the art that means truly nothing at all.  Words that are hollow, that echo cavernous at the slightest knock, that capture us in all these immobile hours with their blank, gratuitous beauty.  These are the months of happy endings and heroics, of riding off into endless sunsets.  Leave the heavy things for the cold.  It will be back soon enough, with the heavy consequences we ignored of our summertime actions and the reminder that we are not, in fact, immortal.

Until then, for a few more weeks still, there is no limit to the pointless hours.  Until then it is okay for it all to mean nothing.

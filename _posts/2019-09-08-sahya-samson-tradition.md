---
layout: post
title: "What Tradition?"
author: "sahya samson"
author_slug: "sahya-samson"
date: 2019-09-08
permalink: /sahya-samson/what-tradition
external_thumbnail_description: "Eurythmy practitioner Sahya Samson on challenges acutely faced by people working in lesser-known forms."
---

There are many things to make an artist question the value of her work. She is always battling with self doubt, worrying about whether it will be appreciated or not, will it sell, will she someday be well-known.

All these forms of outer validation are sought for by most artists, if not all, but they seem to come more easily to some than others.

The most obvious prerequisites for attention is the skill and talent of the artist. But many times artists rise to popularity from purposeful backing, right exposure, smart marketing. Very often talented and quality artists work in the shadows of oblivion.

Artists struggle to achieve outer success for many reasons, but I believe it often comes down to context. Perhaps their work is not recognised because of the time in history they live in; or because their culture or community misunderstands it; maybe the artist’s identity is at odds with the art form or the form itself is highly mysterious and complex and therefore difficult to communicate its worth.

Self-doubt asks the question, _should I care about my art even though no one else does?_

The question really is, if you love your form, are you going to desert it because it likely won’t make you successful? In other words, is the notion of success to come between you and your chosen form?

If you do choose to stick with it, how do you believe in yourself, when no one else seems to? How do you find the strength to keep facing the empty space, knowing there is probably no reward, at least not immediate, at the end of it? No guarantees of audience, opening nights, words of praise.

Is it humanly possible to work on something without appreciation?

<h2 class="fancy-section-title-1">Trusting the Form</h2>

Despite any obstacles the artist of this kind might face, she is able to find immense strength to carry on. I like to think of this artist, toiling away in wintry nonexistence, as the pioneer. Whether beginner or master, the pioneer in my opinion is any artistic soul striving for something new in art.

A pioneering artist is not merely interested in preserving art over centuries as if it were in a museum, but aims for new horizons. What gives him strength is a vision of and trust in these horizons. One cannot find the strength to pursue a difficult path unless there is a vision of what lies ahead.

Isadora Duncan, known as the “Mother of Dance,” abhorred the way dance had developed into something artificial and she sought to return to it that natural grace. After struggling for many years in poverty she set out to Europe, soon to become a world renowned dancer.

Lory Maier Smits (1893---1971) was from Germany and worked alone with dedication for many months on the new art of movement called Eurythmy. Starting with the first tasks in late 1911, she would receive feedback and instruction from her teacher, the Austrian spiritual philosopher Dr. Rudolf Steiner from time to time. She also helped in adding a eurythmy scene to one of his Mystery dramas being staged. Once she began teaching and directing, in 1913, the work grew and performances could take shape. She says on meeting her first pupil and collaborator Annemarie Dubach-Donath at her home in Düsseldorf:

“With her arrival there also ended those sometimes difficult and lonely mornings, during which I had so far worked alone…”

Pioneering artists, whether or not they become famous, are researchers and practitioners at the same time. It seems important to them to hold artistic ideals, have particular tastes and interests and to know what they wish to see in art.

Many years later, there are still dancers carrying on in the manner of Duncan, and there are many performing ensembles in the art of Eurythmy. The hard and lonely work of the individual comes first; then others join the movement, drawn by the artistic vision and expanding on it with every new generation of dancers and eurythmists.

<h2 class="fancy-section-title-1">Outer Success</h2>

While ballet terms, poses, costumes and concerts enchant the young student, the more layered arts like eurythmy, sacred dance or kabuki theatre, leave many people confused with the deep philosophy which is as important to grasp as the gestures and choreography.

I’ve experienced this challenge in trying to spread awareness about Eurythmy. What makes it more difficult to explain is that it has no clear lineage---it doesn't come from dance history. It can be explained from different points: a connection to ancient Greek temple dance, a sculpting of space, a movement art for the stage, some similarities to Tai chi, a movement based on speech and music. It seems that the more I try to explain it, the more I feel like a foreigner who is speaking another language!

Ultimately, it is the experience of the thing which brings about some trickling of understanding. Through my teaching and performing, I see this obscure form become more accessible and even loved.

The art forms that do very well in the world are those that have already reached a peak at some point in their history and continue to live off its rich and accomplished past. What makes them incredibly attractive for learning and pursuing as a career is the fact that they’re polished and finished, readily explained and predictable - they have a winning formula. There is not much mystique about such art forms. But that also means that there may not be as many new discoveries to be made, apart from fusion or bringing in new inspirations. For example, Martha Graham wonderfully brought her love of myth into her ballets.

Sometimes, pioneers seek to open a completely different door in the art world. Pina Bausch’s choreographies did away with beauty or meaningful drama and were instead used for drawing her audience's attention to certain social realities and to learn to think and to question. When I watched the company's Café Müller and Rite of Spring at the BAM, it brought up many reactions from me: I quickly tired of the repetitiveness of automatic motion; I had the strange feeling that I was being mocked for wanting a story with meaning; and I felt uncomfortable watching the dancers fling their bodies as if they were things rather than people.

Then I realised that this is the point: perhaps Baush wants us to share in her questions, in her bewilderment at existence. Perhaps she wants us to feel with full force the meaningless world that she is confronted with and the injustice which makes people into things - she wants us to see the world through her eyes, and to fully feel it too.

<h2 class="fancy-section-title-1">Definition</h2>

As a five or six year old I had no desire to study Bharatnayam, or any other Indian classical dance form for that matter. Although I had some fascination with ballet from a book gifted me, I didn’t want to learn that either. What I had come to love was movement with the veil, an affinity which would later lead to my studies in eurythmy at age nineteen. Eurythmy is as much a spiritual path as a stage art. Its costume is made up of long veil and silk dress that catch the air currents in rippling movement.

I have always felt that one cannot choose one’s art form just for commercial or worldly reasons. One’s art is best chosen for the love of it. For the value and ideal it holds. And if one happens to practice an form that is shrouded in obscurity, one may need to see it like a pioneer does.

The appearance of the working artist’s scene doesn't need to be grand. Sometimes the form, which is like a sapling, will crush under such high expectations. If, at the end of the day, you have rehearsed in the empty room in your home and have gained some new artistic insight or understanding which you write in your journal before bed, that is success. The quality of the work, the trust and patience is essential nourishment to early practice and research.

What makes lesser known forms more exciting than established ones is that there is so much more to discover, always. There are many questions to work with and much insightful material to keep adding.

<h2 class="fancy-section-title-1">Someone has to start it</h2>

Isadora Duncan said, “In [my] school, I shall not teach the children to imitate my movements but to make their own. The primary or fundamental movements of the new school of the dance must have within them the seeds from which will evolve all other movements, each in turn to give birth to others in an unending sequence of still higher and greater expression, thoughts, and ideas."

Dances first arose from close-knit communities with common rituals and wishes. As the group feeling of community disappears with the separating and isolating effect of modern society, there is a rise of the individual. With an artistic vision and ideal set by the individual, groups are drawn to a common dream and something new is created and developed.

Communities now take on the form of dance companies, ensembles, art movements, schools and institutions. What all this takes is deep work of the individual pioneer not looking for short term success, but endeavouring to birth the ever-new and ever-evolving in art.

Every established art form standing on firm history was a young sapling once, nurtured tirelessly by countless generations. And for pioneers, they nurture the form as much as the form nurtures them.


 

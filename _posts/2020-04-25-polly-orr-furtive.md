---
layout: post
title: "In Places We Don't Look"
author: "polly orr"
author_slug: "polly-orr"
date: 2020-04-25
permalink: /polly-orr/furtive-art
external_thumbnail_description: "the secret lives of snow poems"
---
I love being outside after a big snow. Chuck myself into a snowbank. Laugh or scream (depending how I land) to test the muffled resistance of the air. The silence and emptiness of the streets make me feel like I’m the only person in existence. 

Movement catches my attention. 

A stranger fiddling in the snow bank. Their fluorescent orange jacket punctuates the white landscape brilliantly. Worried I’m staring too long, I adjust my gaze back to my side of the street and scurry on. 

As a creature of habit, I stalk the same snow route the next few days. I can’t help but notice the orange clad stranger popping up on this street corner or that driveway. My curiosity progresses from a simmer to a boil as I continue to observe them hovering over a mound of snow before slinking off. One morning, fortune favours me. I find myself on the same side of the street, walking slow, hoping the crunch of the snow won’t alert them to my presence. When I’m a few feet away I see they are holding a stick.

The stick is a wand whisking shapes into the snow. Adjusting my boot straps to buy some time, eventually the stranger finishes and walks off around the corner. I bound over. In the fresh powder that blew in while we slept, a poem is written.

> _what is here will soon be gone. like this poem_  
> _it melts away_  
> _to water the next wave of life_  
> _waiting to emerge_  
> _from muck and mud_

Fascinated, my excitement grows at the thought of more words hidden in other snow banks. I wait a few more minutes to make sure they have a good enough head start. Then I employ my best snow sleuthing and start tracking the footprints through the snow. Every few blocks another poem appears.

> _how strange a privilege_  
> _to be eating orange slices_  
> _while sitting in the snow_

Hunched over my third discovery trying to snap the perfect picture, I feel a tap on my shoulder. I turn around to see the fluorescent orange jacket leaving a glow of colour along the strangers jaw line.

“Hey. I’ve been seeing you around. Thought I’d come say hello. I see you found one of my poems.”

Awkward shyness flutters through my body as I strive to find my voice.

“Uh. Yeah. Totally. Might say I’m a bit of a fan.” A beat of silence. “What gave you the idea to write poems in the snow?”

They smirk and look off into the distance. “Went to art school for a bit and it did some funny things to me. Almost gave up on my practice all together. Then I stumbled on furtive art. It’s left in public places on the sly. The artist ego fucks off for the most part. It’s usually not super obvious who the artist is or even if what you are looking at is art. I think most people walk by these poems and never notice. But I leave dozens of them after every snowfall. I trust that someone eventually sees one. Someone like you.”

We share a smile and I turn back to the poem we are standing beside.

> _as we walk through the crowded_  
> _streets of the city_  
> _we are taught, by osmosis,_  
> _not to notice_  

I ask if they mind me posting a picture on my instagram. They assure me it’s no big deal as long as I don’t mention the specific location. They prefer for people to find the poems more happenstance (if they are found at all). Curiosity peaks in me and I have to ask,

“If you like your poems to be secret, why did you approach me?”

“You seem like you have an artist's soul or some shit. Maybe it’s all the pins on your hat or your bad dye job. Guess I just had a feeling you’d appreciate the jest of it all.” 

We share one last look of understanding and laugh our separate ways home. That day marks the start of a journey into discovering more of the mysterious furtive. What I find along the way marks a tool for resistance, bread crumb trails of impressive commitment, and a cheeky defiance in many of its participants. The main ingredient that seems to bind these practices together is a valuing of the secretive. 

<p class="definition semibold-italic">Furtive art is an artform that covertly inserts itself into public space. Ephemeral, non-spectacular and difficult to detect, furtive art uses strategies of invisibility to achieve its ends.<sup id="footnote-ref-1"><a href="#footnote-1">1</a></sup></p>
 
The signature etched at the bottom of a painting, the dancer’s body twisted under the spotlight, the narrow complimentary font denoting the author's name- in all forms of creation we see the hand of the creator and trace back the genius to its source. The evasive silent side step the furtive aims for captivates me through its rascally push against the norm. 
 
No glory. All motion. All self witness and mediation of meaning from within. Which isn’t to say that this form never reaches out beyond itself and into the lives of others, but that this reaching out is a secondary function, with the fact of its existence being the first. Making art in this way, enacting it in public spaces and alerting no one to its presence, felt to me to be an act of commited solidarity. The artist stands next to their work satiated fully in the act of creation. In an equal act of sovereignty the impact is left to linger in the realm of imagination, the artist never fully grasping what other witnessing or meaning may arise as others bump into, ignore, and ponder what is left behind. 
 
This confidence exuded by many furtive artists of an unknowable impact is epitomized in the work done by [Diane Borsato](https://www.dianeborsato.net/). Coming across her practice was love at first shenanigan. She is a playful performance, visual, and community artist who had many dalliances with the furtive. One of her longest duration pieces was a commitment to lightly brush against or touch 1000 strangers. This performance evolved from research which indicated that behaviour and well being can be impacted through subtle human contact.
 
After discovering this piece, my entire life rippled differently. Alluring thoughts tempted me to believe that there were countless miniscule moments that had slid in and out of my consciousness without my awareness of their full poeticsm. I wandered around for several weeks with new eyes. Was the strange synchronicity of a bright orange pylon parked next to an orange car beside a bed of blooming marigolds just the chaos of life, or was it the carefully constructed brainchild of an installation artist who snuck out to construct the scene before the early morning traffic filtered in from the streets? The idea that creativite expressions could be so integrated into life that they were rendered invisible was beyond seductive. It eventually led me to question the equally invisible assumptions and structures of how we value and validate art. 
 
Does something being art depend on it being made by an artist?
 
Does the work need to be able to be recognized as such?
 
Does our seeing and deciding that something is art make it so?
 
Does the act of being able to see art make someone an artist?

I began to reach out into my own community. I wanted to find some local creatives to share their perspective of working in this form. I run into an interesting paradox fairly quickly. The type of people working in furtive forms are not necessarily keen to be cast into the limelight. Their very relationship to the furtive is often developed as a mechanism to distance themselves from the cult of the artist's personality. With a few deals struck on purposeful vagueness around certain identifying details, I finally find an up and comer willing to chat with me.

Wendy is a Canadian artist in her mid-twenties who blends her spiritual and art practice together. While in conversation it becomes clear that the art she likes to make is not about distinguishing her art practice from the rest of her life. Rather, she seeks to bring more fusion and greyness to these distinctions. Over the past decade she has developed several ceremonial rituals that embody her core values. These rituals can include leaving love letters or small hand crafted gifts around, dancing and singing on behalf of communities in struggle, and sewing seeds of native plant species in alley ways. She does these rituals weekly in public places but specifically chooses abandoned and destitute locales to enact them in. 

Hers is a spirituality with a bite. It’s not simply about seeking the good in life, but finding a way to include the ugly and lonely as equally important aspects. 

“There is a sacredness in all places. Not just temples or galleries or churches. I believe my art brings respect and honour to the places we forget. The defaced and shunned public spaces that every city has. When I show up with my most sacred acts of devotion there, the environment itself becomes my audience and feels my praise.”

When I question the importance of doing these private things in public spaces rather than keeping them within her own space she shares,

“I feel fully engaged by taking on a practice that is done in public places, for a public audience, but is actually about privacy and anonymity. It seems like the ultimate screw you, while maintaining a lighthearted approach. It’s the titillating mischief of being incognito amongst the masses of daily life. A luxury not everyone wants or needs but one I have always valued. Everything we do these days is meant to be on display. What we ate for dinner last night, our feelings about our recent break ups, our opinions of world events happening halfway around the globe. It seems like no one does anything unless there is an audience. So I take everything that I consider to be the most important, the most coveted and sacred parts of my life, take them out where the potential is that anyone could see, but in places where no one actually does. It keeps me humble. How many other miraculous things are happening all the time in places we don’t look?”
 
The furtive has awakened me to these places. There is a great mystery to the business of being alive. While it is common for many art and science practices to seek answers, the furtive invites us to live within the questions. I am grateful for these practices which acknowledge how much goes on behind the scenes that we’ll never know, touch, or see. And then again, some of us will. 

By pure chance any of us may become the unexpected accomplice to the quiet words being published in the snow.

 
<div class="footnotes">
    <ol>
        <li id="footnote-1">
            <a href="#footnote-ref-1">Kathleen Ritter: How to Recognize a Furtive Practice – A User's Guide</a>
        </li>
    </ol>
</div>

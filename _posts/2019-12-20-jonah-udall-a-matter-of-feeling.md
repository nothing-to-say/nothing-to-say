---
layout: post
title: "a matter of feeling"
author: "jonah udall"
author_slug: "jonah-udall"
date: 2019-12-21
permalink: /jonah-udall/a-matter-of-feeling
external_thumbnail_description: "learning about ourselves from what we do and don't like"
description: "learning about ourselves from what we do and don't like"
---

<style>
 .section-break {
     margin-top: 3em;
     margin-bottom: 3em;
     text-align: center;
 }
</style>

On a swampy September afternoon, a couple dozen perfect strangers sat awkwardly around a shrine-like collection of wood, metal and rope contraptions in the center lawn of Manhattan’s Madison Square Park. The pathways and playgrounds were bustling with children, strollers, conversations, but the lawn was serene. The noise washed over us as though from another place, or time.

A trio approached, two forgettable faces flanking a small woman with electric gray hair and firey, yet soft eyes---undeniably from another place or time. Her two companions from Blank Forms, who coordinate the Prismatic Park residency that encompassed this event, introduced Limpe Fuchs: an iconoclast of European experimental music, a radical instrument inventor, and an anarchist trailblazer of public performance art. She nodded “thank you,” whispered a few words entirely lost to the park noise, and stepped among the assembly of her creations.
She looked around at them like a sage among her disciples, or a child among her toys---I couldn’t quite tell which. Without the faintest sense of hurry, she approached one, a metal and string pendulum, proceeding to conjure its sounds which gently sliced through the Manhattan din and held us rapt. She finished, and moved to another instrument, then another, and another.

The organizers told her she should do one more piece. She turned to us and, in a thick, soft German accent, whispered “Which one do you like?”, gesturing to her creations. We were quiet. It was a disarmingly straightforward question. Which one did we find more enjoyable to listen to? She repeated the question, slightly insistent this time. Our silence was as though we felt unequipped to express a simple preference.

<div class="section-break">~</div>

I think sometimes we are afraid of this question. I’ve talked to many artists across disciplines who recall professors outright forbidding pleasure as a justification for a creative decision, or as a meaningful response to another’s work. [I’ve thought a lot](https://nothing-to-say.org/jonah-udall/getting-art.html) about how trying to “get” art---to understand it intellectually---can rob the viewer of the power of felt experience. It hit me that Limpe’s unassuming question is in fact a direct path to felt experience.

Let’s simplify the question further, to just one object: “Do you like it?”

- **You**: Our question is directed to the individual observer. It zeros in on the fundamental, personal relationship between observer and work for an intimate moment. Concerns of style, form, context, medium, etc are set aside.
- **Like**: To everything we experience, we respond spontaneously with liking, disliking or neutral feeling. Our question points here, to our gut---what is the feeling tone of the experience? Though the grammatical object is the work, the observer is being asked about themself. This is where the question radically departs from its toxic and pervasive cousin: “do you judge it as good?”
- **It**: The gestalt. The whole thing. We are not parsing components here---isolating the melody, the shading, the use of metaphor. The “it,” the magic that happens when elements play as one.

I hear the same ethos in many great creatives of Limpe’s generation. Iconic jazz drummer Andrew Cyrille explains an improvised piece to a roomful of students with a long, elaborate, moment-by-moment replay in which the subject and verb of nearly every sentence is simply “I decided.” There is not a “why” in sight. He did what he liked, do you like it? Or to use more mystical language: the muse spoke through him, does it speak to you? I hear it too from Mary Overlie, seminal postmodern choreographer/theater-maker, when she implores “you’re the artist!”, follow what _you_ see, do what _you_ like.

Though this may read as a simplification of the creative task, it is certainly not a trivialization. Embedded in Limpe’s question is the belief that the audience is smart; they don’t have to be _demonstrated_ or _explained_ anything. Overlie speaks of metaphorical piano wires that extend from performer to audience, connecting human experience to human experience. Resonating together to the frequency of “do you like it?”

<div class="section-break">~</div>


So what of when the answer to this question is “no”? It seems sometimes we’re afraid of “no,” that we believe it is a dead end, a fatal pronouncement. Perhaps here is a deeper reason we often avoid the question altogether, not that we think it trivial but that the weight of its answer is too grave. Feeling tone is too primal, too fundamental, to intellectually influence, so it is safer to rely on “appreciation” instead.

As a creator and as a human being, when I answer “no” is when I get most curious about “why.” The question always tells me something about my values, and often points me towards my blindnesses. Sometimes I discover I dislike something because of its presentation, rather than its content. (In certain 80’s pop music, I hear terrific songwriting obscured by overly glossy production.) Sometimes asking “why” might change my feeling, but that’s not the point. I don’t believe there’s anything intrinsically bad about dislike.

Curiosity about our dislikes can also reveal deeper biases. Dislike itself is a natural human experience, but it may emerge from underlying beliefs that don’t hold up to the light. Sometimes shockingly so. I encourage you to take a look for yourself.

<div class="section-break">~</div>

The question of pleasure is important for far simpler reasons, too. As Erin Farrell writes in [this thoughtful piece](https://nothing-to-say.org/erin-farrell/cinephile.html), “we aren’t wrong for what we like.” Art is great---not only powerful, or profound, or important, but just plain great---and if we can’t enjoy it, I think we’re lost.

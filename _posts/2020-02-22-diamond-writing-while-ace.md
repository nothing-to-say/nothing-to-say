---
layout: post
title: "Writing While Ace"
author: "ellie diamond"
author_slug: "ellie-diamond"
date: 2020-02-22
permalink: /ellie-diamond/writing-while-ace
external_thumbnail_description: "How asexuals and aromantics can make relatable art"
---

<style>
 .section-break {
     margin-top: 3em;
     margin-bottom: 3em;
     text-align: center;
 }
</style>

My father was a writer, and I began following in his literary footsteps as soon as I could hold a crayon. At six, I created my first picture book, newsprint and marker stapled together and filed away in my grandmother’s drawer. As I matured, so did my fiction, and by age 12 I was spending hours in front of the family computer, writing my way out of my awkward adolescent existence.

Noticing my developing passion, my father gave me two pieces of advice:

1. Never talk about a work-in-progress, or you’ll never finish.
2. Always write what you know.

I still don’t talk about my work, but I’ve been questioning that second bit of advice for a while.

You see, as a reader, I’ve always gravitated toward books that focused on friendship and adventure over romance and flirtation. I should have suspected why when I was the only girl in seventh grade without a crush, but that was <span class="inverted-text">&lt;year redacted&gt;</span> and “asexual” wasn’t a word—let alone “aromantic.”

After a brief flirtation with outright lesbianism—and a very patient ex-girlfriend—I finally concluded that I am an asexual grey-homoromantic. I went to the internet and there, I found my community. I even learned that a few of my actual three-dimensional friends identified on the ace spectrum. 

For the first time in my life, I felt _real_. I wasn’t just a hollow shell of a human, incapable of “normal” relationships. I was just wired differently. And more importantly, there were others out there that understood. I reveled in it, posting ace memes and pride flag jewelry all over my social media.

Slowly, however, I began to realize that one thing was missing. Books. Specifically, characters that identified similarly to me and were out there living their lives.

When I came out as gay—still an important part of my identity—I was given books that presented beautiful love stories between women. Many are still on my shelves. But the vast majority of them, if not every single one, had descriptions of sexual attraction that continued to leave me cold. The two characters falling into bed together, exploring each other's bodies with explosions of sexual gratification. Whether the love story was F/F or M/F, every time an author gave me this scene as its climax, my heart sank a little bit. Was this the only way a love story could play out? 

And, of course, there was that lingering question that plagues so many people on the ace spectrum: _Will anyone ever want me if I don't want them that way?_

As artists, we want people to be able to see themselves in our work. After all, isn't the purpose of art "to hold, as 'twere, the mirror up to nature?" If I don't see myself in the mirror of art, does it mean that I’m unnatural?

Perhaps more importantly, if I don’t create art that reflects my own experience, am I erasing myself and others like me? That seemed like a highly irresponsible way to create art. I had to know whether it was possible for ace characters to find life in print…and so I turned once again to the internet.

There, I found not only asexual and aromantic characters but also authors who identified on the ace/aro spectrum, and who were willing to talk about their experiences. They had the courage to create characters that reflected their experiences, and those characters gave validation and confidence to people like me.

It was a victory for sure, but it wasn't untarnished. Most of the authors I discovered had been told that their ace characters weren't welcome in mainstream media. 

Asexual/aromantic spectrum author Claudie Arsenault, an advocate for gender and sexual minorities in fiction, has been told by multiple fellow authors that characters who didn’t want sex were [too “boring” to get published](https://theestablishment.co/asexual-authors-speak-out-about-representation-and-ostracization-in-fiction-db60c2e929a2/index.html). Or they’ve heard that a story needed romance if the author wanted a book deal.

Many authors solve this problem by self-publishing, just like filmmakers make indie films and playwrights start theater companies. Going this route gets the work out there, but it also makes the material harder to find. Studio films get into local cinemas while indie films go to specialty houses. Not every community can support a specialty cinema. 
Independent books are sold on Amazon, but you have to know how to look for them. With rare exceptions, no one will see these books on their local bookstore shelves, and if you don't know the book exists, you may never find it. (This is part of why it took me almost a decade to find Claudie Arsenault's work.)

<div class="section-break">~</div>

Things are getting better.

In 2018, for the first time, I saw an ace protagonist smiling up at me from my local bookstore shelf. The book was _Let's Talk About Love_ by Claire Kann, and the character's name is Alice. She's ace, she's homoromantic, _and_ she's a woman of color. 

This gift to underrepresented identities hit the shelves thanks to SwoonReads, an example of an amazing new model in publishing. Authors post their drafts of novels—YA in this case—and they allow readers to rate them and give feedback. Based on that feedback, SwoonReads [grants select books a contract](https://swoonreads.com/faqs/) with Macmillan Publishing.

I saw Alice in the bookstore after I saw her on Goodreads. She was out there, giving a face to an entire identity group. Multiple identity groups, really. And I can’t say whether she was part of a wave of ace characters or if she only made them more noticeable in my eyes. I only know that once I saw her, I started to see more and more like her, in books and in other forms of media.

There was Todd from the Netflix animated series _Bojack Horseman_, who came out as ace in the show's fourth season. Then it seemed like the entire ace community was talking about Jughead from _Archie_, who everyone touted as the first asexual comic book character in the mainstream. Both characters were beacons of hope, ace characters that occupied animated worlds that existed outside of the ace community bubble.

A little bit further out of the mainstream, I found Nancy from Seanan McGuire's _Every Heart a Doorway_ and Nathaniel from Rosiee Thor's _Tarnished Are the Stars_. Both characters represent what I think of as the "next wave" in queer stories: characters whose identities are simply facts of their lives, not the central part of the story. 

Of course, self-discovery is important, but I tend to feel that representation is more validating when characters have lives that don't center around Coming Out. Real people have rich and complex lives that develop beyond their gender and sexual preference identifications. Yes, the fact that I'm ace and on the aro spectrum is an important aspect of my identity, but it's not everything. I also have a life story that includes travel, professional growth, and a beautiful cohort of friends whose starring roles in my life are just as important as any partner's.

I believe that both types of stories are important—Alice's realization that she didn't want sex but did want her girlfriend, as well as Nancy's desperate attempt to find a doorway that will take her back to her true home in the Halls of the Dead. (I highly recommend both books. They're as fascinating as they sound.)

<div class="section-break">~</div>

I'm still searching with fervor, though a bit less desperation, for books that have ace and aro representation. I'm waiting with bated breath for Claudie Arsenault's aro short story anthology, [_Common Bonds_](https://www.goodreads.com/book/show/48159252-common-bonds) (and kicking myself that I missed the submission deadline).

I keep a close eye on [lists of asexuals in fiction](https://www.goodreads.com/list/show/61456.Asexuals_In_Fiction_) and read as much of it as I can, especially if reviewers have said the writing is strong. I can be a bit of a snob about my own art form. I love representation, but I also expect it to be just as well-written as a mainstream cishet book. Otherwise, it doesn't represent us well.

These lists do more than just tell me which books I can read to see my experiences reflected. They remind me that it’s important to write what I know, because people like me are out there looking for stories that reflect them.

As artists, we always have to stretch ourselves and create from experiences outside of our own. We have to draw from what we don’t know as well as what we do, if only to create art that reflects the diversity of the world and the truth of many people’s experiences. 

We just have to make sure our own experiences and truths make it into our art as well.

Now, when I write, I think about the scores of other aces and aros that long to see themselves in stories. I think of how I’ve struggled to see myself as a valid contributor to an art form that seems to center around romance and sex. 

Mostly, though, I think about my artistic voice. I remind myself that only [1% of the population](https://health.usnews.com/health-news/health-wellness/articles/2015/05/04/asexuality-the-invisible-orientation) identifies as asexual. When I create a character that’s ace or aro—when I tell my story through art—I’m doing something that 99% of people can’t do. I’m creating real, fleshed-out characters that make people like me feel _real_.

My worlds look very different now. They’re still populated with plenty of people who probably fall into bed together at the end of the day, but I don’t focus on those scenes. I let other people handle that. I guide my characters as they fight for their friends and families, figure out who they are, and take care of those who need their care.

In short, I allow them to have rich and satisfying lives that mean something to the world around them. Just like I try to do every day.

Turns out my dad was right. The world needs me to write what I know.





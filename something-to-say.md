---
layout: page
permalink: /something-to-say
title: "something to say: anonymous art reviews"
external_thumbnail_description: "why we need anonymous criticism, and how we do it"
---

<h1 class="post-title">something to say</h1>

## _anonymous art reviews_

it's a common trope that the art world is a small one, where we might walk into any show, read any poem, or visit any gallery and more often than not find ourselves within two or one degrees of separation from the artists at work. often these are artists who we some day hope to make a good impression on. usually these are artists above us in power and social influence. consequently, when we have something to say about specific works we are inclined to do it behind closed doors among trusted friends for fear of backlash.

yet these hushed opinions are among the most important for a healthy, introspective, forward-moving community---those where we might challenge the language of a program for shutting out casual audiences, where we might push an environmentally-messaged dance to be more effective in its execution. professional art critics, those insulated from the artists they comment on by nature of their employer, are a dying species. the result, as far as we've seen, is that very little real art commentary is happening today, and that progress may be suffering for it.

_something to say_ is an experiment, hatched out of a series of recent conversations we've had with emerging artists, where we've consistently found people feel safer voicing their actual opinions when they are insulated from potential backlash. we are experimenting with this platform where contributors are invited to submit anonymous reviews written in good faith and with earnest intentions to celebrate what works and challenge what doesn't.

## how it works

1. anyone may submit a review by sending an email to [editors@nothing-to-say.org](mailto:editors@nothing-to-say.org) with a subject prepended `SOMETHING TO SAY:`

- submitted reviews must be at least 500 words, but have no upper length limit
- images and other media inside reviews may be admittable, but will require conversations with the editors.
- note that in general **submissions are not anonymous to the editors**. if the author wishes their submission to be _truly_ anonymous (even to the editors) they may submit via an anonymous email using a service such as [guerrillamail.com](https://www.guerrillamail.com/), but if significant edits are required the submission may be rejected due to the difficulty of anonymous editing back-and-forth. if published, fully anonymous authors cannot be paid since we don't want the IRS thinking we are laundering money.

2. a rotating group of 3 editors from the _something_ editorial board must unanimously approve the submission. to be accepted, it must clearly follow the ground rules specified below. ultimately, the editors may reject a submission for any reason. in any case, we will try to respond within 1 week.
3. if accepted, the editors will work with the author to proofread and edit the submission.
4. upon publication the author will be paid an honorarium of twice the cost they paid to see the thing they reviewed, but no less than $20 and no more than $50. for instance:

- if the author is writing about a show they saw for free, they will be paid $20
- if the review is of a book that cost $15, they will be paid $30
- if the review is of a show that cost $30, they will be paid $50

## ground rules

- if the author feels they could safely share their thoughts with their name attached, they should do so elsewhere
- submissions must be written in good faith
- submissions must not be pedantic
- submissions must be productive.
- submissions must be well-written, but they don't need to be academic or standard prose essays.
- criticisms must be constructive, providing a path toward improvement and lessons that might be learned
- any whiff of an _-ism_ (racism, sexism, etc) will immediately disqualify submissions and the author will receive a rude email back from the editors
- commentary must contribute something reasonably new to the conversation. cheap complaints about not liking a particular style or other well-beaten horses do not belong.
- the work under review must be very recent. we don't strictly define _recent_, but a reasonable guideline is that it should be no older than a month.

## faq

- **aren't anonymous reviews cowardly?** we empathize with how it may seem this way, but we believe for many artists they are unfortunately the safest way to voice certain ideas, particularly when they relate to artists closely in their orbit who have the upper hand in social power dynamics.
- **won't this devolve into a cesspool of negativity and art cynicism?** we are acutely aware of the risks here, which is why we have clearly documented ground rules and why we require unanimous approval from the editorial panel to publish anything. if you do see a published review which you believe violates the ground rules, please let us know at [editors@nothing-to-say.org](mailto:editors@nothing-to-say.org)
- **i am the artist behind a work reviewed here, can i respond here?** no, but you are encouraged to respond by other platforms. if you want, you can let us know and we can pass a link along to the original author.
- **i want to publish a review, but i want my name attached, can i?** no. as a rule, we require that all reviews published here be anonymous. if you have a substantial enough article which goes beyond the bounds of a typical review, you could consider submitting it as a typical _nothing to say_ article.
- **can i request a review of something i've made?** you can email us and we might refer one of our contributors to it, but there are no guarantees.

## masthead

the _something to say_ editorial board consists of:

- [andrew yoon](/contributors.html#andrew-yoon)
- [sequoia sellinger](https://www.sequoiasellinger.com/)
- [amelia mcnally](/contributors.html#amelia-mcnally)
- calvin temoshok

a rotating group of 3 of these editors will preside over all submissions.

note that this editorial board is distinct from the editors of mainline _nothing to say_, which is usually just andrew.

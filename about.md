---
title: about
layout: page
link_text: "?"
permalink: /about
---

<script>
 var RANDOM_STRINGS = ['nothing', 'to', 'say', 'n', 'o', 't', 'h', 'i', 'g', 's', 'a', 'y'];
</script>

> I have nothing to say and I am saying it and that is poetry as I need it
>
> &mdash; <cite>John Cage</cite>

Nothing to Say is a small press based in Brooklyn and Queens. We publish poetry that we love and think you'll love too. With titles forthcoming this spring and summer, we'll be specializing in computational poetry books where each copy is a uniquely generated and printed edition.

As believers in a world without artificial scarcity, everything we publish is licensed under copyleft licenses like the [Creative Commons BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/) license. All our code, including the code behind our computational books, is similarly available under open source licenses.

We strive to give our authors equitable publishing contracts; all authors receive a payment upon signing an agreement, another payment upon delivering a manuscript, and receive 50% of net proceeds from the first sale. Our authors retain copyright ownership of their works, but are required to release their works under copyleft licenses.

Nothing to Say is a registered 501(c)(3) non-profit organization. So far we are entirely self-funded.

Our logo is a square [fermata](https://en.wikipedia.org/wiki/Fermata), used in music notation to indicate a particularly long and indeterminate pause. We hope to share with our readers such moments of pause and reflection. The logo glyph itself is derived from the [Bravura](https://github.com/steinbergmedia/bravura) music font.

If you have nothings to say or know someone who might, we'd love to hear from you! Check out our [submissions page here](/submit).

## PACBI

Nothing to Say endorses the [Palestinian Campaign for the Academic and Cultural Boycott of Israel (PACBI)](https://bdsmovement.net/pacbi) and joins its boycott of institutions complicit in the oppression and genocide of the Palestinian people. Detailed [guidelines on the boycott can be found here](https://bdsmovement.net/pacbi/cultural-boycott-guidelines). This endorsement is made on behalf of the press, not its authors.

## Masthead

<ul class="masthead-list">
	<li><a href="https://andrewyoon.art">Andrew Yoon</a>: codirector, editor</li>
	<li><a href="https://www.sequoiasellinger.com/">Sequoia Sellinger</a>: codirector, marketing director</li>
	<li><a href="/contributors.html#amelia-mcnally">Amelia McNally</a>: codirector</li>
</ul>

<script>
 function shuffleArray(array) {
   // Courtesy of http://stackoverflow.com/a/12646864/5615927
   for (var i = array.length - 1; i > 0; i--) {
     var j = Math.floor(Math.random() * (i + 1));
     var temp = array[i];
     array[i] = array[j];
     array[j] = temp;
   }
   return array;
 }

 function shuffleMasthead() {
   var wrapper = document.getElementsByClassName("masthead-list");
   var entries = wrapper[0].children;
   var shuffledEntries = shuffleArray(Array.prototype.slice.call(entries));
   var newContributorElements = document.createDocumentFragment();
   for (var i = 0; i < shuffledEntries.length; i++) {
     newContributorElements.appendChild(shuffledEntries[i].cloneNode(true));
   }
   wrapper[0].innerHTML = null;
   wrapper[0].appendChild(newContributorElements);
 }

 shuffleMasthead();
</script>

## Previous projects

Nothing to Say began its life in 2017 as an online arts journal where budding artists wrote essays on things they care about. We took a long hiatus during the Covid-19 pandemic, and are now in the process of rebooting with this new focus on publishing poetry books. You can find an [archive of all our published articles here](/archive).

## About this website

This website is built with open source (and mostly [Free](https://www.gnu.org/philosophy/free-sw.html)) software, and is, itself, [open source and Free]({{ site.source_url }}).

We do not run advertisements on this site. We do track anonymized traffic data to see how we're doing. Analytics data is gathered and stored in a privately hosted [Matomo](https://matomo.org/) instance on a $6/mo [DigitalOcean](https://www.digitalocean.com/) VPS we maintain. Since we self-host our analytics, the data is not shared with entities like Google. To opt out, please feel free to use an ad-blocker like [uBlock Origin](https://ublockorigin.com/), since these also block analytics requests.
